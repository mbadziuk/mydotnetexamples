﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Serialization
{
    /*
Создайте пользовательский тип (например, класс) и
     * выполните сериализацию объекта этого типа, учитывая тот факт, что состояние объекта необходимо будет передать по сети. 
 

Задание 2 
Создайте класс, поддерживающий сериализацию. 
     * Выполните сериализацию объекта этого класса в формате XML.
     * Сначала используйте формат по умолчанию, а затем измените его таким образом,
     * чтобы значения полей сохранились в виде атрибутов элементов XML. 
 
Задание 3 
Создайте новое приложение, в котором выполните десериализацию объекта из предыдущего примера. Отобразите состояние объекта на экране. 
    */
    [XmlRoot("Drivers")]
    public class Driver
    {
        [XmlElement("FirstName")]
        public string Name { get; set; }

       
        private int _licenseId;

       
        private bool _hasUnpaidFinesNew;
       
        [XmlAttribute("id")]
        public int LicenseId
        {
            get { return _licenseId; }
            set { _licenseId = value; }
        }

         [XmlElement("Fines")]
        public bool HasUnpaidFines { get; set; }


        public bool HasUnpaidFinesNew
        {
            get { return _hasUnpaidFinesNew; }
            set { _hasUnpaidFinesNew = value; }
        }

        public Driver(string name, int licenseId, bool hasUnpaidFines)
        {
            Name = name;
            LicenseId = licenseId;
            HasUnpaidFines = hasUnpaidFines;
            Aliases = new List<string>();
            Aliases.Add("Morda");
            Aliases.Add("Hobka");
            Aliases.Add("Popka");
        }

        [XmlArray("Psevdonims")]
        [XmlArrayItem("Alias")]
        public List<string> Aliases;

        public Driver()
        {
            
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Driver  driver = new Driver("Max",123,true);
            XmlSerializer serializer = new XmlSerializer(typeof(Driver));
            serializer.Serialize(new FileStream("Drivers.xml",FileMode.Create), driver );


            Console.ReadKey();
        }

    }
}
