#ifndef _ADDRESS_H
#define _ADDRESS_H

#include "ConstantsDefines.h"

namespace ChatClient {
  class Address{

      public:

          Address();
          Address( unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port );
          Address( unsigned int address, unsigned short port );
          unsigned int GetAddress() const;
          unsigned char GetA() const;
          unsigned char GetB() const;
          unsigned char GetC() const;
          unsigned char GetD() const;
          unsigned short GetPort() const;
          void SetMPort(unsigned short _port);
          bool operator == ( const Address & other ) const;
          void operator = ( const Address & other ) ;
          bool operator != ( const Address & other ) const;

      private:

          unsigned int address;
          unsigned short port;
  };
}


#endif
