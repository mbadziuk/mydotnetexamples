// ChatClient.cpp : Defines the entry point for the console application.
//
#include "EasySocket.h"
#include "ConstantsDefines.h"
#include "Address.h"
#include "Message.pb.h"

//#define LOCAL

int main(int argc, char* argv[]) {
  using namespace ChatClient;
  using namespace datainterchange;

  int isClient = 0;
  int isDistant = 0;
  unsigned short portSend = 0;
  unsigned short portListen = 0;
  int msgCounter = 1;

  EasySocket hostSocket;
  Address localIpAddressReceive(127,0,0,1, 50000);
  Address localIpAddressSend(127,0,0,1, 50001);
 
  Address pcIpAddressReceive(10,10,10,88, 50000);
  Address boardIpAddressSend(10,10,10,45, 50001);

  ChatClient::Address sendAddress;
  ChatClient::Address receiveAddress;

  printf("\nChoose socket port Listen: ");
  scanf("%d", &portListen);
  printf("Choose socket port Destination: ");
  scanf("%d", &portSend);
  printf("1 - Distant connection \n0 - Local connection\nChoice: ");
  scanf("%d", &isDistant);
  printf("1 - Client \n0 - Server\nChoice: ");
  scanf("%d", &isClient);

  // configure socket params
  if (isClient == 1 && isDistant == 1) {
    receiveAddress = pcIpAddressReceive;
    sendAddress = boardIpAddressSend;
    hostSocket.ChangeRole(true);
  } else if (isClient == 0 && isDistant == 1) {
    receiveAddress = boardIpAddressSend;
    sendAddress = pcIpAddressReceive;
  }  else if (isClient == 1 && isDistant == 0) {
    receiveAddress = localIpAddressReceive;
    sendAddress = localIpAddressSend;
    hostSocket.ChangeRole(true);
  } else if (isClient == 0 && isDistant == 0) {
    receiveAddress = localIpAddressSend;
    sendAddress = localIpAddressReceive;
  }
  receiveAddress.SetMPort(portListen);
  sendAddress.SetMPort(portSend);
  hostSocket.Open(receiveAddress.GetPort());  
  
  //  Communication loop HalfDuplex
  while(true) {
    if (hostSocket.GetRole()) { // true - client
        char packet[1024];
        char msg[1024];
        int packet_size = 0;
        
        printf("==> ");
      
        // get user chat message
        scanf("%s", &packet);

        // get packet size
        for (packet_size = 0; packet[packet_size] != '\0'; packet_size++);  
        packet_size++;

        printf("\n");

        // create Message for protobuf interchange and populate it
        Message myMessage;
        myMessage.set_id(msgCounter++);
        myMessage.set_from("From: Client\n");
        myMessage.set_to("To: Server\n");
        myMessage.set_email("E-mail: client@like.programming\n");
        Message::Content* content = myMessage.add_innercontent();
        content->set_type(Message::ContentType::Message_ContentType_PLAINTEXT);
        content->set_content(packet, sizeof(packet_size));
        
        // simple send char buffer
        // hostSocket.Send(sendAddress, packet, sizeof(packet));


        // serialized protobuf send
        int msgSize = myMessage.ByteSize();
        void *buffer = malloc(msgSize);
        myMessage.SerializeToArray(buffer, msgSize);
        hostSocket.Send(sendAddress, buffer, msgSize);
        free(buffer);
        
        if (hostSocket.EndSession(packet)) {
          break;
        }
      } else {     // server
        Address sender;
        char packet[1024];
        hostSocket.Receive(sender, packet, 1024);
      }
   }
    
   printf("\n\nSession finished !");
   getchar();
  
   return 0;
}


