#include "Address.h"

using namespace ChatClient;

Address::Address(){
  address = ( 127 << 24 ) | ( 0 << 16 ) | ( 0 << 8 ) | 1;   // 127.0.0.1
  port = 50000;
}

Address::Address( unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short port ): port(port) {
 address = ( a << 24 ) | ( b << 16 ) | ( c << 8 ) | d;   // 10.10.10.88
}

Address::Address( unsigned int address, unsigned short port ): address(address), port(port) {}


void Address::SetMPort(unsigned short _port) {
  port = _port;
}
unsigned int Address::GetAddress() const {
  return address;
}
unsigned char Address::GetA() const {
  return address << 24;
}
unsigned char Address::GetB() const {
  return (address << 16) & 255;
}
unsigned char Address::GetC() const {
  return (address << 8) & 255;
}
unsigned char Address::GetD() const {
  return address & 255;
}
unsigned short Address::GetPort() const {
  return port;
}
bool Address::operator == ( const Address & other ) const {
  return address == other.address && port == other.port;

}
void Address::operator = ( const Address & other ) {
  this->address = other.address;
  this->port = other.port;
}

bool Address::operator != ( const Address & other ) const {
  return !operator==(other);
}

