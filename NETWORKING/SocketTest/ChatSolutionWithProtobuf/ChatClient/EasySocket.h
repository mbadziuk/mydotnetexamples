#ifndef _EASYSOCKET_H
#define _EASYSOCKET_H

#include "Platform.h"
#include "Address.h"
#include "Message.pb.h"

#if PLATFORM == PLATFORM_WINDOWS
  #pragma comment( lib, "wsock32.lib" )
#endif

namespace ChatClient {
  using namespace datainterchange;
  class EasySocket
      {
      public:
          static int socketsCounter;
          bool role;

          EasySocket();
          ~EasySocket();
          bool Open( unsigned short port );
          void Close();
          bool IsOpen() const;
          bool Send( const Address & destination, const void * data, int size );
          int Receive( Address & sender, void * data, int size );
          bool GetRole();
          void ChangeRole(bool _role);
          void Init(int packetSize, bool _role);
          bool EndSession(const char* message);
          void PrintProtobufMessage(Message* msg);

      private:

          int handle;

          bool initializeSockets();
          void shutdownSockets();
      };
}
#endif