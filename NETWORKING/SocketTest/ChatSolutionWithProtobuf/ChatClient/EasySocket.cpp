#include "EasySocket.h"
#include "Address.h"
#include "ConstantsDefines.h"
#include "Message.pb.h"
#include <string>
#include <iostream>

#if PLATFORM == PLATFORM_WINDOWS
  #include "io.h"
#endif

using namespace ChatClient;
using namespace datainterchange;

void EasySocket::ChangeRole(bool _role) {
  role = _role;
  //role = role ? false : true;

} // EasySocket::changeRole

bool EasySocket::initializeSockets(){
  if (socketsCounter == 0) {
    #if PLATFORM == PLATFORM_WINDOWS
      WSADATA WsaData;
      return WSAStartup( MAKEWORD(2,2), &WsaData ) == NO_ERROR;
    #else
      return true;
    #endif
  } //if

  socketsCounter++;
} // EasySocket::initializeSockets()

void EasySocket::shutdownSockets() {
  socketsCounter--;
  if (socketsCounter == 0) {
    #if PLATFORM == PLATFORM_WINDOWS
      WSACleanup();
    #endif
  } //if
}// EasySocket::shutdownSockets

EasySocket::EasySocket() {
  Init(256, false);
}// EasySocket::EasySocket

EasySocket::~EasySocket(){
  shutdownSockets();
}// EasySocket::~EasySocket

bool EasySocket::GetRole() {
  return role;
} // EasySocket::getCurrentRole

void EasySocket::Init(int maxPacketSize, bool _role){
  initializeSockets();

  role = _role;
    
  // create base socket
  handle = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );

  if ( handle <= 0 ) {
    printf( "failed to create socket\n" );
    throw;
  }
}

bool EasySocket::Open( unsigned short port ) {
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons( (unsigned short) port );
 
  // bind port to socket (main Socket on this machine)
  if ( bind ( handle,                     // socket
              (const sockaddr*) &address,       // address structure
              sizeof(sockaddr_in) ) < 0          // size
             ) {
    printf( "failed to bind socket\n" );
       throw;
  }
  
  // making socket to work in non-blocking mode
#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
  int nonBlocking = 1;
  if ( fcntl( handle, F_SETFL, O_NONBLOCK, nonBlocking ) == -1 ) {
      printf( "failed to set non-blocking socket\n" );
      throw;
  }
#elif PLATFORM == PLATFORM_WINDOWS
  DWORD nonBlocking = 1;
  if ( ioctlsocket( handle, FIONBIO, &nonBlocking ) != 0 ) {
    printf( "failed to set non-blocking socket\n" );
    throw;
  }
#endif
}

void EasySocket::Close() {
  close(handle);
  shutdownSockets();
}

int EasySocket::Receive(Address & sender, void * data, int size) {
  unsigned int from_address = 0;
  unsigned int from_port = 0;
  char* data_ptr = (char*) data;
  int received_bytes = 0;
  int i = 0;
  
  while ( true ) {
    #if PLATFORM == PLATFORM_WINDOWS
      typedef int socklen_t;
    #endif

    sockaddr_in from;
    socklen_t fromLength = sizeof( from );
    
    received_bytes = recvfrom( 
                                handle,
                                data_ptr,
                                size, 
                                0,
                                (sockaddr*)&from,
                                &fromLength );

    if ( received_bytes <= 0 )
        continue;

    from_address = ntohl( from.sin_addr.s_addr );
    from_port = ntohs( from.sin_port );
          
    sender = Address(from_address, from_port);

    // process received packet
    printf("<== ");


    // protobuf deserialize packet
    Message message;
    Message::Content content;
    message.ParseFromArray(data_ptr, size);

    PrintProtobufMessage(&message);

    // simple print
    /*
    for (i = 0; data_ptr[i] != '\0' && i < size; i++) {
      printf("%c", data_ptr[i]);
    } // for
    */

    printf("\n");
    break;
  } // while

  ChangeRole(!role);

  return true;
}// EasySocket::receive

void EasySocket::PrintProtobufMessage(Message* msg) {
  int id = msg->id();
  const datainterchange::Message_Content& content = msg->innercontent(0);
  const std::string within = content.content();

  printf("Message Id => %d\n", id);
  std::cout << within << std::endl;
//  wprintf(L"Content => %s", content.content());
 // wprintf(L"Content => %s", within);

  //for (i = 0; data_ptr[i] != '\0' && i < size; i++) {
  //  printf("%c", data_ptr[i]);
  //} // for

}
bool EasySocket::Send(const Address & destination, const void * data, int size){
  bool result = false;
  int sent_bytes = 0;
  const char* data_ptr = (const char*)data;

  if (role) { // client
    
    sockaddr_in address;
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl( destination.GetAddress() );
    address.sin_port = htons( destination.GetPort() );

    int sent_bytes = sendto (
                            handle,
                            (const char*)data, 
                            size,
                            0,
                            (sockaddr*)&address,
                            sizeof(sockaddr_in) 
                            );

    if ( sent_bytes != size ) {
      printf( "failed to send packet: return value = %d\n", sent_bytes );
    } else {
      result = true;    
    } // if

    ChangeRole(!role);
  } // if
  return result;
} // EasySocket::send

bool EasySocket::EndSession(const char* message) {
  char exit[5] = "exit";
  bool isSessionEnd = false;
  int counter = 0;
  int i = 0; 

  for (i = 0; message[i] == exit[i]; i++, counter++);

  if (counter == 5)
    isSessionEnd = true;

  return isSessionEnd;
}

int EasySocket::socketsCounter = 0;