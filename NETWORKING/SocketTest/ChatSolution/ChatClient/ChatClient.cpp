// ChatClient.cpp : Defines the entry point for the console application.
//
#include "EasySocket.h"
#include "ConstantsDefines.h"
#include "Address.h"

//#define LOCAL

int main(int argc, char* argv[]) {
  using namespace ChatClient;

  int isClient = 0;
  int isDistant = 0;
  unsigned short portSend = 0;
  unsigned short portListen = 0;

  EasySocket hostSocket;
  Address localIpAddressReceive(127,0,0,1, 50000);
  Address localIpAddressSend(127,0,0,1, 50001);
 
  Address pcIpAddressReceive(10,10,10,88, 50000);
  Address boardIpAddressSend(10,10,10,45, 50001);

  ChatClient::Address sendAddress;
  ChatClient::Address receiveAddress;

  printf("\nChoose socket port Listen: ");
  scanf("%d", &portListen);
  printf("Choose socket port Destination: ");
  scanf("%d", &portSend);
  printf("1 - Distant connection \n0 - Local connection\nChoice: ");
  scanf("%d", &isDistant);
  printf("1 - Client \n0 - Server\nChoice: ");
  scanf("%d", &isClient);

  // configure socket params
  if (isClient == 1 && isDistant == 1) {
    receiveAddress = pcIpAddressReceive;
    sendAddress = boardIpAddressSend;
    hostSocket.ChangeRole(true);
  } else if (isClient == 0 && isDistant == 1) {
    receiveAddress = boardIpAddressSend;
    sendAddress = pcIpAddressReceive;
  }  else if (isClient == 1 && isDistant == 0) {
    receiveAddress = localIpAddressReceive;
    sendAddress = localIpAddressSend;
    hostSocket.ChangeRole(true);
  } else if (isClient == 0 && isDistant == 0) {
    receiveAddress = localIpAddressSend;
    sendAddress = localIpAddressReceive;
  }
  receiveAddress.SetMPort(portListen);
  sendAddress.SetMPort(portSend);
  hostSocket.Open(receiveAddress.GetPort());  
  
  //  Communication loop HalfDuplex
  while(true) {
    if (hostSocket.GetRole()) { // true - client
        char packet[1024];
        int packet_size = 0;

        printf("==> ");
      
        // get user chat message
        scanf("%s", &packet);

        // get packet size
        for (packet_size = 0; packet[packet_size] != '\0'; packet_size++);  
        packet_size++;

        printf("\n");

        hostSocket.Send(sendAddress, packet, sizeof(packet));

        if (hostSocket.EndSession(packet)) {
          break;
        }
      } else {     // server
        Address sender;
        char packet[1024];
        hostSocket.Receive(sender, packet, 1024);
      }
   }
    
   printf("\n\nSession finished !");
   getchar();
  
   return 0;
}


