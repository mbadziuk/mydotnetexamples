// ChatClient.cpp : Defines the entry point for the console application.
//
#include "EasySocket.h"
#include "ConstantsDefines.h"
#include "Address.h"
#include <iostream>

//#define LOCAL
#define PORT 8888
/*
int main(int argc, char* argv[]) {
  
  char a = std::getchar();

  if (a == '0')
  {
	WSADATA WsaDat;
	if(WSAStartup(MAKEWORD(2,2),&WsaDat)!=0)
	{
		std::cout<<"WSA Initialization failed!\r\n";
		WSACleanup();
		return 0;
	}
	
	SOCKET Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(Socket==INVALID_SOCKET)
	{
		std::cout<<"Socket creation failed.\r\n";
		WSACleanup();
		return 0;
	}
	
	SOCKADDR_IN serverInf;
	serverInf.sin_family=AF_INET;
	serverInf.sin_addr.s_addr=INADDR_ANY;
	serverInf.sin_port=htons(PORT);

	if(bind(Socket,(SOCKADDR*)(&serverInf),sizeof(serverInf))==SOCKET_ERROR)
	{
		std::cout<<"Unable to bind socket!\r\n";
		WSACleanup();

		return 0;
	}

	listen(Socket,1);

	SOCKET TempSock=SOCKET_ERROR;
	while(TempSock==SOCKET_ERROR)
	{
		std::cout<<"Waiting for incoming connections...\r\n";
		TempSock=accept(Socket,NULL,NULL);
	}
	Socket=TempSock;

	std::cout<<"Client connected!\r\n\r\n";

	char *szMessage="Welcome to the server!\r\n";
	send(Socket,szMessage,strlen(szMessage),0);

	// Shutdown our socket
	shutdown(Socket,SD_SEND);

	// Close our socket entirely
	closesocket(Socket);

	// Cleanup Winsock
	WSACleanup();

	return 0;
  } else {
	// Initialise Winsock
	WSADATA WsaDat;
	if(WSAStartup(MAKEWORD(2,2),&WsaDat)!=0)
	{
		std::cout<<"Winsock error - Winsock initialization failed\r\n";
		WSACleanup();

		return 0;
	}
	
	// Create our socket
	SOCKET Socket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(Socket==INVALID_SOCKET)
	{
		std::cout<<"Winsock error - Socket creation Failed!\r\n";
		WSACleanup();
	
		return 0;
	}

	// Resolve IP address for hostname
	struct hostent *host;
	if((host=gethostbyname("localhost"))==NULL)
	{
		std::cout<<"Failed to resolve hostname.\r\n";
		WSACleanup();
		
		return 0;
	}

	// Setup our socket address structure
	SOCKADDR_IN SockAddr;
	SockAddr.sin_port=htons(PORT);
	SockAddr.sin_family=AF_INET;
	SockAddr.sin_addr.s_addr=*((unsigned long*)host->h_addr);

	// Attempt to connect to server
	if(connect(Socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr))!=0)
	{
		std::cout<<"Failed to establish connection with server\r\n";
		WSACleanup();
		
		return 0;
	}

	// Display message from server
	char buffer[1000];
	memset(buffer,0,999);
	int inDataLength=recv(Socket,buffer,1000,0);
	std::cout<<buffer;

	// Shutdown our socket
	shutdown(Socket,SD_SEND);

	// Close our socket entirely
	closesocket(Socket);

	// Cleanup Winsock
	WSACleanup();

	return 0;
}
 
}
*/

int main(int argc, char* argv[]) {
  using namespace ChatClient;

  int isClient = 0;
  int isDistant = 0;
  unsigned short portSend = 0;
  unsigned short portListen = 0;

  EasySocket hostSocket;
  Address localIpAddressReceive(127,0,0,1, 50000);
  Address localIpAddressSend(127,0,0,1, 50001);
 
  Address pcIpAddressReceive(10,10,10,88, 50000);
  Address boardIpAddressSend(10,10,10,45, 50001);

  ChatClient::Address sendAddress;
  ChatClient::Address receiveAddress;

  printf("\nChoose socket port Listen: ");
  scanf("%d", &portListen);
  printf("Choose socket port Destination: ");
  scanf("%d", &portSend);
  printf("1 - Distant connection \n0 - Local connection\nChoice: ");
  scanf("%d", &isDistant);
  printf("1 - Client \n0 - Server\nChoice: ");
  scanf("%d", &isClient);

  // configure socket params
  if (isClient == 1 && isDistant == 1) {
    receiveAddress = pcIpAddressReceive;
    sendAddress = boardIpAddressSend;
    hostSocket.ChangeRole(true);
  } else if (isClient == 0 && isDistant == 1) {
    receiveAddress = boardIpAddressSend;
    sendAddress = pcIpAddressReceive;
  }  else if (isClient == 1 && isDistant == 0) {
    receiveAddress = localIpAddressReceive;
    sendAddress = localIpAddressSend;
    hostSocket.ChangeRole(true);
  } else if (isClient == 0 && isDistant == 0) {
    receiveAddress = localIpAddressSend;
    sendAddress = localIpAddressReceive;
  }
  receiveAddress.SetMPort(portListen);
  sendAddress.SetMPort(portSend);
  hostSocket.SetRole(isClient);

  hostSocket.Open(receiveAddress.GetPort(), sendAddress);  
  
  //  Communication loop HalfDuplex
  while(true) {
    if (hostSocket.GetRole()) { // true - client
        char packet[1024];
        int packet_size = 0;

        printf("==> ");
      
        // get user chat message
        scanf("%s", &packet);

        // get packet size
        for (packet_size = 0; packet[packet_size] != '\0'; packet_size++);  
        packet_size++;

        printf("\n");

        hostSocket.Send(packet, sizeof(packet));

        if (hostSocket.EndSession(packet)) {
          break;
        }
      } else {     // server
        Address sender;
        char packet[1024];
        hostSocket.Receive(packet, 1024);
      }
   }
    
   printf("\n\nSession finished !");
   getchar();
  
   return 0;
}

