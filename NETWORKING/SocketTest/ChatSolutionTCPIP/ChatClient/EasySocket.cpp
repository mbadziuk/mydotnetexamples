#include "EasySocket.h"
#include "Address.h"
#include "ConstantsDefines.h"

#if PLATFORM == PLATFORM_WINDOWS
  #include "io.h"
#endif

using namespace ChatClient;

void EasySocket::ChangeRole(bool _role) {
  role = _role;
  //role = role ? false : true;

} // EasySocket::changeRole

bool EasySocket::initializeSockets(){
  if (socketsCounter == 0) {
    #if PLATFORM == PLATFORM_WINDOWS
      WSADATA WsaData;
      return WSAStartup( MAKEWORD(2,2), &WsaData ) == NO_ERROR;
    #else
      return true;
    #endif
  } //if

  socketsCounter++;
} // EasySocket::initializeSockets()

void EasySocket::shutdownSockets() {
  socketsCounter--;
  if (socketsCounter == 0) {
    #if PLATFORM == PLATFORM_WINDOWS
      WSACleanup();
    #endif
  } //if
}// EasySocket::shutdownSockets

EasySocket::EasySocket() {
  connectedClientSocketDesc = 0;
  handle = 0;
  Init(256, false);
}// EasySocket::EasySocket

EasySocket::~EasySocket(){
  shutdownSockets();
}// EasySocket::~EasySocket

bool EasySocket::GetRole() {
  return role;
} // EasySocket::getCurrentRole
void EasySocket::SetRole(int _role) {
  if (_role == 1) 
    role = true;
  
  isServer = role ? false : true;
} // EasySocket::getCurrentRole

bool EasySocket::GetLength() {
  return len;
} // GetLength
int EasySocket::GetConnectedClientSocketDesc() {
  return connectedClientSocketDesc;
} // EasySocket::GetConnectedClientSocketDesc

void EasySocket::Init(int maxPacketSize, bool _role){
  initializeSockets();

  role = _role;
  isServer = _role ? false : true;
    
  // create base socket
 // handle = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP ); 
  handle = socket( AF_INET, SOCK_STREAM, IPPROTO_TCP ); 


  if ( handle <= 0 ) {
    printf( "failed to create socket\n" );
    throw;
  }
}

bool EasySocket::Open( unsigned short port, Address &serverAddr ) {
  sockaddr_in client;
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons( (unsigned short) port );
  int result = 0;
  address.sin_port = htons(8888);//serverAddr.GetPort());

  if (GetRole()) {
    struct hostent *host;
    host=gethostbyname("localhost");

    memset(&client,0,sizeof(client));
    address.sin_addr.s_addr = *((unsigned long*)host->h_addr);
    //address.sin_addr.s_addr = serverAddr.GetAddress();
    address.sin_port = htons(8888);//serverAddr.GetPort());

    result = connect(handle,(struct sockaddr*)&address,sizeof(address)); 

    if (result <0)
      printf("Connection wasn't establihsed.");
  } else {
    // bind port to socket (main Socket on this machine)
    if ( bind ( handle,                     // socket
                (const sockaddr*) &address,       // address structure
                sizeof(sockaddr_in) ) < 0          // size
               ) {
      printf( "failed to bind socket\n" );
         throw;
    }

    if ( listen ( handle, 1) ) {
      printf( "failed to listen to socket\n" );
         throw;
    }
    len = sizeof(client);
    connectedClientSocketDesc = accept(handle, NULL, NULL);

    printf("Client connected!\r\n\r\n");

    char *szMessage="Welcome to the server!\r\n";
    send(connectedClientSocketDesc,szMessage,strlen(szMessage),0);

  }
 
  // making socket to work in non-blocking mode
#if PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
  int nonBlocking = 1;
  if ( fcntl( handle, F_SETFL, O_NONBLOCK, nonBlocking ) == -1 ) {
      printf( "failed to set non-blocking socket\n" );
      throw;
  }
  if (isServer) {
    if (  fcntl( handle, F_SETFL, O_NONBLOCK, nonBlocking ) != 0 ) {
      printf( "failed to set non-blocking socket\n" );
      throw;
    }
  }
#elif PLATFORM == PLATFORM_WINDOWS
  DWORD nonBlocking = 1;
  if ( ioctlsocket( handle, FIONBIO, &nonBlocking ) != 0 ) {
    printf( "failed to set non-blocking socket\n" );
    throw;
  }
  if (isServer) {
    if ( ioctlsocket( connectedClientSocketDesc, FIONBIO, &nonBlocking ) != 0 ) {
      printf( "failed to set non-blocking socket\n" );
      throw;
    }
  }

#endif

}

void EasySocket::Close() {
  if (handle != 0) 
    close(handle);
  if (connectedClientSocketDesc != 0) 
    close(connectedClientSocketDesc);
  shutdownSockets();
}

int EasySocket::Receive( void * data, int size) {
  unsigned int from_address = 0;
  unsigned int from_port = 0;
  char* data_ptr = (char*) data;
  int received_bytes = 0;
  int i = 0;
  int socket;

  if (isServer) {
    socket = connectedClientSocketDesc;
  } else {
    socket = handle;
  }

  while ( true ) {
    #if PLATFORM == PLATFORM_WINDOWS
      typedef int socklen_t;
    #endif

    sockaddr_in from;
    socklen_t fromLength = sizeof( from );

    received_bytes = recv(socket, data_ptr,1024,0);  

    if ( received_bytes <= 0 )
        continue;

    // process received packet
    printf("<== ");

    for (i = 0; data_ptr[i] != '\0' && i < size; i++) {
      printf("%c", data_ptr[i]);
    } // for

    printf("\n");
    break;
  } // while

  ChangeRole(!role);

  return true;
}// EasySocket::receive

bool EasySocket::Send(const void * data, int size){
  bool result = false;
  int sent_bytes = 0;
  const char* data_ptr = (const char*)data;
  int socket;

  if (isServer) {
    socket = connectedClientSocketDesc;
  } else {
    socket = handle;
  }

  sent_bytes = send(socket, data_ptr, 1024, 0);     
    
  if ( sent_bytes != size ) {
    printf( "failed to send packet: return value = %d\n", sent_bytes );
  } else {
    result = true;    
  } // if

  ChangeRole(!role);
  
  return result;
} // EasySocket::send

bool EasySocket::EndSession(const char* message) {
  char exit[5] = "exit";
  bool isSessionEnd = false;
  int counter = 0;
  int i = 0; 

  for (i = 0; message[i] == exit[i]; i++, counter++);

  if (counter == 5)
    isSessionEnd = true;

  return isSessionEnd;
}

int EasySocket::socketsCounter = 0;