#ifndef _EASYSOCKET_H
#define _EASYSOCKET_H

#include "Platform.h"
#include "Address.h"

#if PLATFORM == PLATFORM_WINDOWS
  #pragma comment( lib, "wsock32.lib" )
#endif


namespace ChatClient {
  class EasySocket
      {
      public:
          static int socketsCounter;
          bool role;
          bool isServer;
          
          EasySocket();
          ~EasySocket();
          bool Open( unsigned short port, Address &address );
          void Close();
          bool IsOpen() const;
          bool Send( const void * data, int size );
          int Receive(void * data, int size );
          bool GetRole();
          void SetRole(int role);
          bool GetLength();
          int GetConnectedClientSocketDesc();
          void ChangeRole(bool _role);
          void Init(int packetSize, bool _role);
          bool EndSession(const char* message);

      private:

          int handle;
          int connectedClientSocketDesc;
          int len;

          bool initializeSockets();
          void shutdownSockets();
      };
}
#endif