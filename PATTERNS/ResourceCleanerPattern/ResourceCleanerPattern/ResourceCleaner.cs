﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceCleanerPattern
{
    class ResourceCleaner:IDisposable
    {
        private bool _isDisposed = false;

        public void Dispose()
        {
            CleanResources(true);

            GC.SuppressFinalize(this);
        }

        ~ResourceCleaner()
        {
            CleanResources(false);
        }

        private void CleanResources(bool disposeClean)
        {
            if (! _isDisposed)
            {
                if (disposeClean)
                {
                    Console.WriteLine("Clear all !!!");
                }


            }
            _isDisposed = true;
        }
    }
}
