﻿using System;
using System.Collections.Generic;
using System.Text;
// Реализация полиморфизма времени выполнения программы 3-мя способами (извлечение интерфейса, использование dynamic,...)
namespace AdHocPolymorphism
{
    
    class Program
    {
        static void Main(string[] args)
        {
            dynamic[] ar = {new MyClass1(), new MyClass2(), new MyClass3()};

            foreach (var @interface in ar)
            {
                @interface.Method(1);
            }

            Console.ReadKey();
            

        }
    }

    class MyClass1
    {
        public void Method(int num)
        {
            Console.WriteLine(this.GetType() + " => Method");
        }
    }
    class MyClass2
    {
        public void Method(int num)
        {
            Console.WriteLine(this.GetType() + " => Method");
        }
    }
    class MyClass3
    {
        public void Method(int num)
        {
            Console.WriteLine(this.GetType() + " => Method");
        }
    }
}
