﻿using System;
using System.Collections.Generic;
using System.Text;

// НеВиртуальный Интерфейс - NonVirtual Interface implemented
namespace TemplateMethodAkaNonVirtualInterface
{
    class Car
    {
        public void MakeCarOperational()
        {
            AddFuel();
            StartEngine();
            SwitchGears(2);
        }

        protected virtual void AddFuel()
        {
            Console.WriteLine("General Fuel added..");
        }
        protected virtual void StartEngine()
        {
            Console.WriteLine(" Key Turned and Engine started...");
        }
        protected virtual bool SwitchGears(int a)
        {
            Console.WriteLine("Switch from Neutral to Drive... moving...");
            if (a%2 == 0)
                return true;
            else
                return false;
        }

        public virtual bool DoSome(int a)
        {
            Console.WriteLine("DoSOmeNethod !!!");
            return true;
        }
    }

    class SimpleCar : Car
    {
        public new void DoSome()
        {
            Console.WriteLine("My own Do Some!!!");
        }

        public override bool DoSome(int a)
        {
            if (a % 2 == 0)
                return true;
            else
                return false;
        }
    }

    class Tractor : Car
    {
        protected override void AddFuel()
        {
            Console.WriteLine("Tractor=  Add Solyara !!!");
        }

        protected override bool SwitchGears(int a)
        {
            Console.WriteLine("Tractor= Valve pull... drive...");
            if (a % 2 == 0)
                return true;
            else
                return false;
        }

        public override bool DoSome(int a)
        {
            if (a % 2 == 0)
                return true;
            else
                return false;
        }
    }

    class SportCar  : Car
    {
        protected override void StartEngine()
        {
            Console.WriteLine(" Sport Car Button pressed and Engine started...");
        }

        protected override bool SwitchGears(int a)
        {
            Console.WriteLine("SC On Wheel switcher gear switched ... vjjjj...");
            if (a % 2 == 0) 
                return false;
            else
                return true;
        }

        public override bool DoSome(int a)
        {
            if (a % 2 == 0)// ERROR in this class ->test should find it
                return true;
            else
                return false;
        }
    }
}
