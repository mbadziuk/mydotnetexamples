﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace TemplateMethodAkaNonVirtualInterface
{
    [TestFixture]
   public class TestCar
    {
        [Test]
        public void TestDoSomeSimpleCar()
        {
            Car car = new SimpleCar();
            int param = 2;
            bool result = car.DoSome(param);

            Assert.AreEqual(true, result);

        }

        [Test]
        public void TestDoSomeSportCar()
        {
            Car car = new SportCar();
            int param = 2;
            bool result = car.DoSome(param);

            Assert.AreEqual(true, result);

        }

        [Test]
        public void TestDoSomeCar()
        {
            Car car = new Car();
            int param = 2;
            bool result = car.DoSome(param);

            Assert.AreEqual(true, result);
         }

        [Test]
        public void TestDoSomeTractor()
        {
            Car car = new Tractor();
            int param = 2;
            bool result = car.DoSome(param);

            Assert.AreEqual(true, result);

        }

    }
}
