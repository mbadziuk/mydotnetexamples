﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ProcessInspector
{
    /*
     * 
     * Создайте консольное приложение, которое в различных потоках сможет получить доступ к 2-м файлам. 
     * Считайте из этих файлов содержимое и попытайтесь записать полученную информацию в третий файл. 
     * Чтение/запись должны осуществляться одновременно в каждом 
        из дочерних потоков. Используйте блокировку потоков для того, чтобы добиться корректной записи в конечный файл. 
     */

    class SynchronizedReaderWriter
    {
        private readonly string _source;
        private TextWriter _destination;
        private Thread thread;
        
        [ThreadStatic]
        static string data;

        public Thread WorkerThread {get { return thread; } }

        public SynchronizedReaderWriter(string source, TextWriter destination)
        {
            _source = source;
            _destination = destination;

            thread = new Thread(Worker);
            thread.Start();
        }

        private void Worker()
        {
            if (File.Exists(_source))
                data = File.ReadAllText(_source);

            lock (_destination)
            {
                _destination.Write(data);
                _destination.Flush();
            }
        }
        
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            FileStream stream = new FileStream("destination.log",FileMode.Create, FileAccess.Write);
            SynchronizedReaderWriter work1 = new SynchronizedReaderWriter("source_1.txt", new StreamWriter(stream));
            SynchronizedReaderWriter work2 = new SynchronizedReaderWriter("source_2.txt", new StreamWriter(stream));
            
            work1.WorkerThread.Join();
            work2.WorkerThread.Join();
            stream.Close();

            Console.ReadKey();
        }

        private static void ProcessKill()
        {
            Process[] runningPrograms = Process.GetProcesses();

            SortedList<int, Process> list = new SortedList<int, Process>();

            foreach (Process program in runningPrograms)
            {
                if (program.ProcessName.ToLower() == "chrome")
                {
//                    program.Kill();
                }

                list.Add(program.Id, program);
            }

            foreach (KeyValuePair<int, Process> pair in list)
            {
                Console.WriteLine("Id {0} , Name {1}", pair.Key, pair.Value.ProcessName);
            }

            Process skype = list.Values.FirstOrDefault(process => process.ProcessName.ToLower() == "skype");

            if (skype != null)
            {
                Console.WriteLine("Skype have found  ! Kill it ?");
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.Y)
                {
                    skype.Kill();
                    Console.WriteLine("Killing done )");
                }
            }
        }
    }
}