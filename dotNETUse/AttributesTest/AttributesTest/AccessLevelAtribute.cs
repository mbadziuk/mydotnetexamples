﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttributesTest
{
    [AttributeUsage(AttributeTargets.Class)]
    class AccessLevelAtribute : System.Attribute
    {
        public enum AccessLevelEnum
        {
            Guest, User, Director, Admin
        }

        public AccessLevelEnum AccessLevel { get; set; }

        private int _id;

        public AccessLevelAtribute(int id)
        {
            _id = id;
        }

        public string GetAccessRights()
        {
            return AccessLevel.ToString();
        }
    }
}
