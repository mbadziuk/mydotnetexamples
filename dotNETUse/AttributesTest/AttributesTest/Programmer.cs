﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttributesTest
{
    [AccessLevelAtribute(2, AccessLevel = AccessLevelAtribute.AccessLevelEnum.Admin)]
    class Programmer:Employee
    {
    }
}
