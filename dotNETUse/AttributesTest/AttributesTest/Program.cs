﻿#define STANDARD

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

/*
 
Создайте пользовательский атрибут AccessLevelAttribute,
 * позволяющий определить уровень доступа пользователя к системе.
 * Сформируйте состав сотрудников некоторой фирмы в виде набора классов, 
 * например, Manager, Programmer, Director. 
 * При помощи атрибута AccessLevelAttribute распределите
 * уровни доступа персонала и отобразите на экране реакцию системы 
 * на попытку каждого сотрудника получить доступ в защищенную секцию. 
 
 2 
Создайте класс и примените к его методам атрибут Obsolete сначала в форме просто
 * выводящей предупреждение, а затем в форме, препятствующей компиляции. 
 * Продемонстрируйте работу атрибута на примере вызова данных методов.  
 
 3 
Расширьте возможности программы-рефлектора из предыдущего урока следующим образом: 
1. Добавьте возможность выбирать, какие именно члены типа должны быть показаны пользователю. 
 * При этом должна быть возможность выбирать сразу несколько членов типа, например, методы и свойства. 
2. Добавьте возможность вывода информации об атрибутах для типов и всех членов типа, которые могут быть декорированы атрибутами. 
*/


namespace AttributesTest
{
    class Program
    {
        static void Main(string[] args)
        {
            GetVersionOfLibrary();



            Console.ReadKey();
        }

        private static void GetVersionOfLibrary()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            Console.WriteLine("Name => {0}", assembly.FullName);

            AssemblyName name = assembly.GetName();

            Console.WriteLine("Version => {0}", name.Version);

        }

        private static void PreAttributesTest()
        {
            AttrTest t = new AttrTest();

            t.MethodPremium();
            t.MethodStandard();
            t.Method();
        }

        private static void EmployeeMetaAccessLevelTest()
        {
            List<Employee> list = new List<Employee>();
            Employee director = new Director();
            Employee manager = new Manager();
            Employee programmer = new Programmer();
            Employee guset = new Guest();
            list.Add(director);
            list.Add(manager);
            list.Add(programmer);
            list.Add(guset);

            foreach (Employee employee in list)
            {
                ProtectedSection(employee);

                Console.WriteLine(new string('*', 20));
            }
        }

        static void ProtectedSection(Employee employee)
        {
            object[] a = employee.GetType().GetCustomAttributes(typeof (AccessLevelAtribute), false);
            AccessLevelAtribute accessLevel = ParseAttributes(a);

            if (accessLevel == null)
            {
                Console.WriteLine("User has no right in system => access denied !!!");
            }
            else
            {
                Console.WriteLine(" User is => {0} and has this access rights => {1}.", employee.GetType().Name, accessLevel.GetAccessRights());
            }
        }

        static AccessLevelAtribute ParseAttributes(object[] array)
        {
            foreach (var v in array)
            {
                if (v is AccessLevelAtribute)
                    return v as AccessLevelAtribute;
            }
            return null;
        }
    }

    class AttrTest
    {
        [Obsolete("I'm an old method )) don't use me! Use New Method instead !!")]
        public void OldMethod()
        {
            Console.WriteLine("..");
        }

        [Obsolete("Old Method!!! Error ", true)]
        public void OldMethod1()
        {
            Console.WriteLine("..");
        }

        [Conditional("PREMIUM")]
        public void MethodPremium()
        {
            Console.WriteLine("PREMIUM method... do smth...");
        }

        [Conditional("STANDARD")]
        public void MethodStandard()
        {
            Console.WriteLine("STANDARD method... do smth...");
        }

#if DEBUG
        public void Method()
        {
            Console.WriteLine("Debug method... do smth...");
        }
#else
        public void Method()
        {
            Console.WriteLine("RELEASE method... do smth...");
        }
#endif

    }
}
