﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace WordChanger
{
    class Program
    {
        private static string _assemblyName;
        private static int _count;
        private static string _nominalLogName = "_bnrCashe.log";
        private static bool deleteNominalLogs = false;

        private static Regex pattern1 = new Regex(@">система<");
        private static Regex pattern2 = new Regex(@">bundlerDivСистемаCycleCount<");
        private static Regex pattern3 = new Regex(@">fullСистемаStatus<");

        
        static void Main(string[] args)
        {
            System.Console.Clear();
            Console.Write("Do you want to delete Nominal logs [Y/y]: ");
            char deleteFileOption = Convert.ToChar(System.Console.Read());

            // set delete nominal logs option, false by default
            if (Char.IsLetter(deleteFileOption) && (deleteFileOption == 'y' || deleteFileOption == 'Y')) {
                deleteNominalLogs = true;
                Console.WriteLine("\nNominal logs will be deleted.\n");
            }//if

            _assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".exe";

            DirectoryInfo currrentDir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            DirProcessor(currrentDir);

            Console.ReadKey();
        }

        private static void DirProcessor(DirectoryInfo dir)
        {
            FileProcessor(dir.GetFiles().ToList<FileInfo>());

            DirectoryInfo[] dirs = dir.GetDirectories();

            if (dirs.Length == 0) {
                return;
            }

            foreach (var dirr in dirs) {
                DirProcessor(dirr);
            }
        }

        private static void FileProcessor(List<FileInfo> files)
        {
            foreach (FileInfo file in files)
            {

                // delete _bnrCashe.log file
                if (deleteNominalLogs && file.FullName.Contains(_nominalLogName)) {
                    file.Delete();
                    continue;
                }//if

                // skip if not audit report
                if (file.FullName.Contains(_assemblyName) || Path.GetExtension(file.FullName) != ".mei" )
                {
                    continue;
                }

                String result = Replacement(File.ReadAllText(file.FullName, Encoding.GetEncoding("windows-1251")));
                File.WriteAllText(file.FullName, result, Encoding.GetEncoding("windows-1251"));
                Console.WriteLine("File processed: {0}", file.Name);
            }
        }

        static string Replacement(string check) 
        {
            //Console.WriteLine(pattern.Replace("serfe>< sdfsdf.>><dfsfdf> dsfsdfsd< sdfdsf>  >система< іваівафп пвап sdfsdf ", ">system<"));

            String result1 = pattern1.Replace(check, ">system<");
            String result2 = pattern2.Replace(result1, ">bundlerDivSystemCycleCount<");

            return pattern3.Replace(result2, ">fullSystemStatus<");
        }


    }

}
