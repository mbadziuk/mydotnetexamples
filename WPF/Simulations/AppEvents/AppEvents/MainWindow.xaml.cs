﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppEvents
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            throw new Exception("I throwed this Exception !!!");
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
             // open
            NewMainWnd newDoc = new NewMainWnd();

            newDoc.Owner = this;

            (Application.Current as App).WndCollection.Add(newDoc);
            newDoc.Show();
                

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            // update
            foreach (var item in (Application.Current as App).WndCollection)
            {
                item.Content = DateTime.Now;
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            // close
            int r = new Random().Next((Application.Current as App).WndCollection.Count);

            NewMainWnd toClose = (Application.Current as App).WndCollection[r];
            toClose.Close();

            
        }

        private void cmd_CreateThread_Click_1(object sender, RoutedEventArgs e)
        {
            Thread potok2 = new Thread(newThread);
            potok2.Start();
        }

        private void newThread() {
            Thread.Sleep(3000);

            for (int i = 10; i > 1; i--) {
                Thread.Sleep(1000);
                tBlock_1.Dispatcher.BeginInvoke((Action)delegate() { tBlock_1.Text = i.ToString(); });
            }

            cmd_Exception.Dispatcher.BeginInvoke((Action)delegate() { this.Button_Click_1(cmd_Exception, null); });

        }
    }
}
