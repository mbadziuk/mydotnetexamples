﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace AppEvents
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public List<NewMainWnd> WndCollection = new List<NewMainWnd>();

        public App() {
            this.DispatcherUnhandledException += App_DispatcherUnhandledException;
            this.StartupUri = new Uri("MainWindow.xaml", System.UriKind.Relative);

            
        }

        void App_DispatcherUnhandledException(
            object sender, 
            System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message.ToString());
            e.Handled = true;
        }

        
        protected override void OnActivated(EventArgs e)
        {
           base.OnActivated(e);
        
           this.MainWindow.Width = 600;
        }

        protected override void OnDeactivated(EventArgs e)
        {
           base.OnDeactivated(e);

           this.MainWindow.Width = 500;

          }
    }
}
