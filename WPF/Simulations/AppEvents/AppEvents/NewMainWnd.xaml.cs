﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppEvents
{
    /// <summary>
    /// Interaction logic for NewMainWnd.xaml
    /// </summary>
    public partial class NewMainWnd : Window
    {
        public NewMainWnd()
        {
            InitializeComponent();
        }

        private void Window_Closed_1(object sender, EventArgs e)
        {
            (Application.Current as App).WndCollection.Remove(this);
        }
    }
}
