﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DependencyPropertyTrial
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SliderInfo myFirDepProp = new SliderInfo();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnClearTB_Click_1(object sender, RoutedEventArgs e)
        {

            myFirDepProp.SliderValue = (int)sldrSpeed.Value;

            tbkSliderValue.Text = myFirDepProp.SliderValue.ToString();
           // MessageBox.Show(myFirDepProp.SliderValue.ToString());
        }

        

       
    }
}
