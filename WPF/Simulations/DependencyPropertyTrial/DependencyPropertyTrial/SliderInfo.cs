﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace DependencyPropertyTrial
{
    class SliderInfo : DependencyObject {
        
        // Using a DependencyProperty as the backing store for SliderValue.  This enables animation, styling, binding, etc...
        public static DependencyProperty SliderValueProperty;


        public int SliderValue
        {
            get { return (int)GetValue(SliderValueProperty); }
            set { SetValue(SliderValueProperty, value); }
        }

        static FrameworkPropertyMetadata metadata = new FrameworkPropertyMetadata(new CoerceValueCallback(Correct), new PropertyChangedCallback(Change));

        private static void Change(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Application.Current.MainWindow.Title = e.NewValue.ToString();

        }

        private static object Correct(DependencyObject d, object baseValue)
        {
            if ((int)baseValue > 50)
            {
                return 50;
            }
            return baseValue;
        }

        static SliderInfo() {
              SliderValueProperty = DependencyProperty.Register("SliderValue", typeof(int), typeof(SliderInfo), metadata, new ValidateValueCallback(Validate));
        }

        private static bool Validate(object value)
        {
            
            return true;
        }
    }

}
