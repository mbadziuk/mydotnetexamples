﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LoginPopup
{
    /// <summary>
    /// Interaction logic for ConfirmationPopup.xaml
    /// </summary>
    public partial class ConfirmationPopup : Window
    {
        public ConfirmationPopup()
        {
            InitializeComponent();
            
        }

        private void btnYes_Click_1(object sender, RoutedEventArgs e)
        {
            if (sender is Button) {
                if ((sender as Button).Content.ToString().Equals("Yes")) {
                   MessageBox.Show("OK ! Now you were registered !");
                } else {
                    MessageBox.Show(" Registration process cancelled !");
                }
                this.Close();
            }
        }
    }
}
