﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LoginPopup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Title = "Registration";
        }

        private void brnConfirm_Click_1(object sender, RoutedEventArgs e)
        {
            if (   ! String.IsNullOrEmpty(tbxLogin.Text)
                && ! String.IsNullOrEmpty(tbxLogin.Text)) {

                    MessageBoxResult result = MessageBox.Show("Confirm", "Are you sure?", MessageBoxButton.YesNo);

                    if (result == MessageBoxResult.Yes) {
                        ConfirmationPopup confirmPp = new ConfirmationPopup();
                        confirmPp.ShowDialog();
                    }


            }
        }
    }
}
