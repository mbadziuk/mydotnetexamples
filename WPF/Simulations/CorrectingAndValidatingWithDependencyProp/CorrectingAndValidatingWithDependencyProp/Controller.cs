﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace CorrectingAndValidatingWithDependencyProp
{
    class Controller : FrameworkElement
    {
        public int CheckNumbersAndSetSliderMax
        {
            get { return (int)GetValue(CheckNumbersAndSetSliderMaxProperty); }
            set { SetValue(CheckNumbersAndSetSliderMaxProperty, value); }
        }

        static PropertyMetadata metadata = new PropertyMetadata(0, new PropertyChangedCallback(OnPropertyChanged),new CoerceValueCallback(CorrectValue));

        private static object CorrectValue(DependencyObject d, object baseValue)
        {
            return (int)((int)baseValue / 2 * 12);
        }

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
        }

        // Using a DependencyProperty as the backing store for CheckNumbersAndSetSliderMax.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CheckNumbersAndSetSliderMaxProperty =
            DependencyProperty.Register("CheckNumbersAndSetSliderMax", typeof(int), typeof(Controller), metadata, new ValidateValueCallback(ValidateValue));

        private static bool ValidateValue(object value)
        {
           // return  ((int)value < 5000);
            return true;
        }

        
    }
}
