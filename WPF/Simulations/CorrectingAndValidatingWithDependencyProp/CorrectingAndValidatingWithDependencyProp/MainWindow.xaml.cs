﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CorrectingAndValidatingWithDependencyProp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void tbxNumbers_PreviewKeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space) {
                e.Handled = true;
            }
        }

        private void tbxNumbers_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            if (!isNumber(e.Text)) {
                e.Handled = true;
            }
        }

        private void tbxUpperCase_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            if (isNumber(e.Text) || Char.IsLower(e.Text[0])) {
                e.Handled = true;
            }
        }

        private bool isNumber(string input) { 
            short result;
            return Int16.TryParse(input, out result);
        }
    }
}
