﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace BubbledRoutedEvent
{
    class UserButton : Button
    {
        public static RoutedEvent userClickEvent;

        static UserButton() {
            userClickEvent = EventManager.RegisterRoutedEvent("userClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UserButton));
        }

        public event RoutedEventHandler userClick {
            add { AddHandler(userClickEvent, value); }
            remove { RemoveHandler(userClickEvent, value); }
        }
    }
}
