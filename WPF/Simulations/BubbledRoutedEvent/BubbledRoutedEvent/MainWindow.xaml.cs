﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BubbledRoutedEvent
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            int width = 250;
            int height = 250;
            int step = 20;
            Button root = new Button();
            this.Content = root;


            for (; width > 20;) {
                UserButton btn = new UserButton();
                btn.Style = (Style)this.Resources["ButtonStyle1"];
                btn.userClick +=  new RoutedEventHandler(Button_Click_1);
                btn.Width = width;
                btn.Height = height;
                width -= step;
                height -= step;
                root.Content = btn;
                root = btn;
            }

            
        //   root.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Thread.Sleep(50);
            byte R = (byte) new Random().Next(7);
            SolidColorBrush[] arr = new SolidColorBrush[] { Brushes.SeaShell, Brushes.Lavender, Brushes.IndianRed, Brushes.Blue, Brushes.AliceBlue, Brushes.Aquamarine, Brushes.BlanchedAlmond, Brushes.DarkMagenta };

            (sender as Button).Background  = arr[R];
            
        }
    }
}
