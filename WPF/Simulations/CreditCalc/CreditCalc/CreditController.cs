﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace CreditCalc
{
    class CreditController : FrameworkElement
    {
        public int MaximumCreditSum
        {
            get { return (int)GetValue(MaximumCreditSumProperty); }
            set { SetValue(MaximumCreditSumProperty, value); }
        }

        static PropertyMetadata  metadata = new PropertyMetadata(0, new PropertyChangedCallback(OnPropertyChanged), new CoerceValueCallback(OnCorrectValue));

        // Using a DependencyProperty as the backing store for MaximumCreditSum.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximumCreditSumProperty =
            DependencyProperty.Register("MaximumCreditSum", typeof(int), typeof(CreditController), metadata, new ValidateValueCallback(ValidateValueCallbackMethod));

        private static bool ValidateValueCallbackMethod(object value)
        {
            return (int)value >= 0;
        }

        private static object OnCorrectValue(DependencyObject d, object baseValue)
        {
            CreditController a = (CreditController)d;
            


            int possibleCredit = (int)((int)baseValue / 2 * 12);

            return possibleCredit;
        }

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        } 
        
        
    }
}
