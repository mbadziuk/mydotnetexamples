﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreditCalc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        EventModel _event = new EventModel() {EventName = "Sons Birthday !" };

        public MainWindow()
        {
            InitializeComponent();

            EventUI.DataContext = _event;

        }

        private void PreviewTextInputMonthlyIncome(object sender, TextCompositionEventArgs e)
        {
            if (tbxIncome.Text.Length > 5) {
                e.Handled = true;
            } 

            short result;
            bool isNumber = Int16.TryParse(e.Text, out result);

            if ( !isNumber) {
                e.Handled = true;
            }
        }

        private void PreviewKeyDownIncome(object sender, KeyEventArgs e) {
            if (e.Key == Key.Space) {
                e.Handled = true;
            }
        }

        private void tbxOnlyUp_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            short result;
            bool isNumber = Int16.TryParse(e.Text, out result);

            if (isNumber || (Char.IsUpper(e.Text[0])))
            {
                e.Handled = true;
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _event.EventName = new Random().Next(999).ToString();
        }


    }
}
