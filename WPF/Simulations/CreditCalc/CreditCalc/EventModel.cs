﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace CreditCalc
{
    class EventModel : INotifyPropertyChanged
    {
        private string _eventName;

        public string EventName
        {
            get { return _eventName; }
            
            set {
                _eventName = value;

                if (PropertyChanged!=null)
                    PropertyChanged(this, new PropertyChangedEventArgs("EventName"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
