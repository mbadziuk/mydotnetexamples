﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MenuCommands
{
    class CheckKey : FrameworkElement, ICommand
    {
        static MainWindow win;

        

        public bool CanExecute(object parameter)
        {
            win = App.Current.MainWindow as MainWindow;

            return !win.tbxInputKey.Text.Equals("Ведите ключ...")
                  && win.tbxInputKey.Text != String.Empty;
        }

        public void Execute(object parameter)
        {
            win.canExecute = win.tbxInputKey.Text.Equals("abracadabra");
        }

        // Событие происходит при изменениях, которые могут     повлиять на возможность запуска команды.
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class DataCommandsLibrary
    {
        // Пользовательская команда.
        private static RoutedUICommand requery;

        static DataCommandsLibrary()
        {
            // InputGestureCollection - горячие клавиши, на которые команда будет реагировать.
            InputGestureCollection inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.R, ModifierKeys.Control, "Ctrl+R"));

            requery = new RoutedUICommand("Requery", "Requery", typeof(DataCommandsLibrary));//, inputs);
            // 1 параметр: Текст, который будет отображаться, если команда будет присвоена пункту меню.
            // 2 параметр: Имя команды.
            // 3 параметр: Класс объявляющий команду.
            // 4 параметр: Горячие клавиши.
        }

        public static RoutedUICommand Requery
        {
            get { return requery; }
        }

        public static void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("Произведен повторный запрос.");
        }
    }
}
