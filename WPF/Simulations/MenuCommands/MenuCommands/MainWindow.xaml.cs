﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MenuCommands
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool canExecute = false;
        
        public MainWindow()
        {
            InitializeComponent();

            //CheckKey key = new CheckKey(this);
            //CommandBinding bind = new CommandBinding(key);
            //CommandBindings.Add(bind);
            //CheckKey.Command = key;
        }

        private void disableCommandHandler(object sender, ExecutedRoutedEventArgs e)
        {
            canExecute = !canExecute;
        }

        private void CommandBinding_CanExecute_1(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = canExecute;
        }

        private void CommandBinding_Executed_New(object sender, ExecutedRoutedEventArgs e)
        {
            String s = e.Parameter.ToString();
            bool? result = false;
            switch (s)
            {
               case "New":
                    tbxInput.Text = "";
                    break;
               case "Open":
                    OpenFileDialog windowOpen = new OpenFileDialog();
                    result = windowOpen.ShowDialog();
                    
                    if (result == true) {
                        tbxInput.Text = windowOpen.FileName;
                    }
                    
                    break;
               case "Save":
                    SaveFileDialog sf = new SaveFileDialog();
                    result = sf.ShowDialog();

                    if (result == true)
                    {
                        tbxInput.Text = sf.FileName;
                    }

                    break;

                default:
                    break;
            }
        }
      
    }
}
