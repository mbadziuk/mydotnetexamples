﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace MenuCommands
{
    class CustomCommand : ICommand
    {
        MainWindow win;

        public CustomCommand(MainWindow refer) {
            this.win = refer;
        }

        public bool CanExecute(object parameter)
        {
            return win.canExecute;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            win.tbxInput.Text = (String)parameter; 
        }

    }
}
