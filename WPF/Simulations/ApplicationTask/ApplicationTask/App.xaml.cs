﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;

namespace ApplicationTask
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal static StringBuilder appEvents = new StringBuilder();

        public App()
        {
            this.Startup += new StartupEventHandler(StartUp);
            this.Activated += new EventHandler(OnActivatedL);
            this.Deactivated += new EventHandler(OnDectivatedL);
        }

        private static void StartUp(object sender, StartupEventArgs e)
        {
            appEvents.Append("Start\n");
            Debug.WriteLine("---------->");
        }
        
        protected override void OnExit(ExitEventArgs e)
        {
            appEvents.Append("Exit\n");
        }

        private static void OnActivatedL(object sender, EventArgs e)
        {
            appEvents.Append("Activated\n");
        }

        private static void OnDectivatedL(object sender, EventArgs e)
        {
            appEvents.Append("Dectivated\n");
        }

        
    }
}
