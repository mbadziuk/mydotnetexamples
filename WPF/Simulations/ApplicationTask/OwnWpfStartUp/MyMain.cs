﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace OwnWpfStartUp
{
    class MyMainApp : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Console.WriteLine("!!!! Started :)");
            base.OnStartup(e);
        }

        [STAThread]
        public static void Main(string[] args) {

            MyMainApp app = new MyMainApp();
            app.StartupUri = new System.Uri("MainWindow.xaml", UriKind.Relative);

            app.Run();
        }
    }
}
