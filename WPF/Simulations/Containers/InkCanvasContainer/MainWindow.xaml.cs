﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InkCanvasContainer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // Выборка всех режимов редактирования InkCanvas.
            foreach (InkCanvasEditingMode mode in Enum.GetValues(typeof(InkCanvasEditingMode)))
                cbxChoice.Items.Add(mode);

            cbxChoice.SelectedItem = icInk.EditingMode;
        }

        private void lbxChoice_Selected_1(object sender, RoutedEventArgs e)
        {
            ListBoxItem item = (ListBoxItem)lbxChoice.SelectedItem;
            

            if (item.Name == "lbxiDraw")
            {
                icInk.EditingMode = InkCanvasEditingMode.Ink;
            }
            else {
                icInk.EditingMode = InkCanvasEditingMode.EraseByStroke;
            }
        }

       

    }
}
