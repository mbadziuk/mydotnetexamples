﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace CreditCalculator
{
    class SimpleProperty : FrameworkElement
    {
        private static int  CreditMaximum = 0;
        public string Incomes
        {
            get { return (string)GetValue(IncomesProperty); }
            set { SetValue(IncomesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Income.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IncomesProperty =
            DependencyProperty.Register("Incomes", typeof(string), typeof(SimpleProperty), new FrameworkPropertyMetadata(new PropertyChangedCallback(OnChangePossibleCredit)));

        private static void OnChangePossibleCredit(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            int monthInc = Int32.Parse(e.NewValue.ToString());

            int cr = (int)(monthInc * 0.5 * 12);
            CreditMaximum = cr;
            ((SimpleProperty)d).SliderMaximum = CreditMaximum;
        }

        public int SliderMaximum
        {
            get { return (int)GetValue(SliderMaximumProperty); }
            set { SetValue(SliderMaximumProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SliderMaximum.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SliderMaximumProperty =
            DependencyProperty.Register("SliderMaximum", typeof(int), typeof(SimpleProperty), new PropertyMetadata(0));

        
     


    }
}
