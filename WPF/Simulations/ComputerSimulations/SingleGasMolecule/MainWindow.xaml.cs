﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SingleGasMolecule
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Boolean isRunning = false;
        int moleculeRadius = 50;
        Random rand = new Random();
        double angle = 0;
        double Speed = 0;
        double SpeedX = 0;
        double SpeedY = 0;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (isRunning)
            {
                StartStopBtn.Content = "Start";
                isRunning = false;
            }
            else {
                StartStopBtn.Content = "Stop";
                isRunning = true;
                GasMolecule.Width  = moleculeRadius;
                GasMolecule.Height = moleculeRadius;
                angle = rand.NextDouble() * 2 * Math.PI;
                Speed = rand.Next(1, 20);
                SpeedX = Speed * Math.Cos(angle);
                SpeedY = Speed * Math.Sin(angle);

                Canvas.SetLeft(GasMolecule, rand.Next(moleculeRadius, (int)MainCanvas.Width - moleculeRadius ));
                Canvas.SetTop(GasMolecule, rand.Next(moleculeRadius, (int)MainCanvas.Height - moleculeRadius));
            }

            while (isRunning) {
                double X = Canvas.GetLeft(GasMolecule) + SpeedX;
                double Y = Canvas.GetTop(GasMolecule) + SpeedY;

                if (X > MainCanvas.Width || X < 0) {
                    SpeedX *= -1;
                }
                if (Y > MainCanvas.Height|| Y < 0)
                {
                    SpeedY *= -1;
                }

                Canvas.SetLeft(GasMolecule, X);
                Canvas.SetTop(GasMolecule, Y);
                
            }


        }
    }
}
