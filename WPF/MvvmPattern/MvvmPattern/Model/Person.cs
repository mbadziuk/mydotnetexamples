﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace MvvmPattern
{
    class Person : INotifyPropertyChanged
    {
        private string _firstName;
        private string _secondName;
        private int _homePhone;
        private int _age;
        
        public string FirstName
        {
            get { return _firstName; }

            set { _firstName = value; 
                OnPropertyChanged("FirstName"); }
        }

        public string SecondName
        {
            get { return _secondName; }

            set { _secondName = value; OnPropertyChanged("SecondName"); }
        }

        public int HomePhone
        {
            get { return _homePhone; }

            set { _homePhone = value; OnPropertyChanged("HomePhone"); }
        }

        public int Age
        {
            get { return _age; }

            set { _age = value; OnPropertyChanged("Age"); }
        }

        public Person(string fname, string sname,int phone,int age) 
        {
            FirstName = fname;
            SecondName = sname;
            HomePhone = phone;
            Age = age;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string pname) {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(pname));
        }
    }
}
