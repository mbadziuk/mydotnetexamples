﻿using MvvmPattern.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace MvvmPattern
{
    class PersonsViewModel: INotifyPropertyChanged 
    {
        public PersonsViewModel()
        {
            _persons = new ObservableCollection<Person>();
            _persons.Add(new Person("Maxim", "Badzyuk", 24234, 36));
            _persons.Add(new Person("Anton", "Egorov", 22222, 23));
        }

        private ObservableCollection<Person> _persons;
        public ObservableCollection<Person> Persons 
        {
            get { return _persons; }
            set { _persons = value; OnPropertyChanged("Persons"); }

        }

        private Person _selectedPerson;
        public Person SelectedPerson {
            get { return _selectedPerson; }
            set { _selectedPerson = value; OnPropertyChanged("SelectedPerson"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string pname)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(pname));
        }


        #region Command

        private DelegateCommand _addPersonCommand;
        public DelegateCommand AddPersonCommand
        {
            get
            {
                return _addPersonCommand ?? (_addPersonCommand = new DelegateCommand(AddNewPerson));
            }
        }

        private void AddNewPerson(object arg)
        {
            if (IsAddAllowed)
                _persons.Add (new Person("John", "Doe", 00000, 22 ) );
        }
       
        private bool _isAddAllowed;
        public bool IsAddAllowed 
        {
            get { return _isAddAllowed; }
            set { _isAddAllowed = value; OnPropertyChanged("IsAddAllowed"); }
        }

        #endregion

    }

}
