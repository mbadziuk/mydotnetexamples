﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;


namespace TryWpf
{
    class Temperature:INotifyPropertyChanged
    {
        int _currentTemperature;

        public int CurrentTemperature
        {
            get { return _currentTemperature; }
            set { _currentTemperature = value; OnPropertyChanged("CurrentTemperature"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }

    class ConvertCelToFar : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            float celciusTemp = float.Parse(value.ToString());
            float kelvinTemp = ((9 / 5) * celciusTemp) + 32;

            return (int)kelvinTemp;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            float kelvinTemp = float.Parse(value.ToString());
            float celciusTemp = (kelvinTemp - 32) / 1.8f;

            return (int)kelvinTemp;
        }


    }

}
