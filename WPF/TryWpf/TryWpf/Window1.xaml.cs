﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using TryWpf.DataM;
using System.Xml;
using System.Collections;
using System.Windows.Media.Animation;

namespace TryWpf
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        NickNames names;
        

      
        public Window1()
        {
           InitializeComponent();
           names = new NickNames();
           dockPanel.DataContext = names;

           CommandBinding newCmdBind = new CommandBinding(ApplicationCommands.New);
           newCmdBind.Executed += newCmdBind_Executed;
           CommandBindings.Add(newCmdBind);

           pwd.LostFocus += pwd_PasswordChanged;

           combo.ItemsSource = this.FindResource("family") as Family;
       
           //spc.DataContext = person;
           
           // Animation samples 
           Storyboard anime = (Storyboard)this.FindResource("animes");
           //anime.Begin();
        DoubleAnimation dl = new DoubleAnimation(0,400, new Duration(new TimeSpan(100000000)));
//         myEllipse.BeginAnimation(Ellipse.WidthProperty, dl);
            
        }

        void pwd_PasswordChanged(object sender, RoutedEventArgs e)
        {
            
        }

        void newCmdBind_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBox.Show("New File command invoked  " + e.OriginalSource.ToString());
        }

        void Window1_MouseMove(object sender, MouseEventArgs e)
        {
           
            
        }

        void nickTxb_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("TextBlock clicked !!! " + e.OriginalSource.ToString());
        }

        void Window1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            
               MessageBox.Show("Main Window double clicked !!! Event come from element = " + e.OriginalSource.ToString());
        }

        void addButton_Click(object sender, RoutedEventArgs e)
        {
            this.names.Add(new NickName("John", "Doe"));
        }

        private void Window1_MouseMove(object sender, RoutedEventArgs e)
        {
            Person person = (Person)this.FindResource("hey");


           MessageBox.Show("We change the Person." + person.Name + "  " + person.Age);
        }

        private void Back_Click_1(object sender, RoutedEventArgs e)
        {
            Family family = this.FindResource("family") as Family;
            ICollectionView view = CollectionViewSource.GetDefaultView(family);
            view.MoveCurrentToPrevious();
            if (view.IsCurrentBeforeFirst)
                view.MoveCurrentToLast();
        }

        private void Next_Click_1(object sender, RoutedEventArgs e)
        {
            Family family = this.FindResource("family") as Family;
            ICollectionView view = CollectionViewSource.GetDefaultView(family);
            view.MoveCurrentToNext();
            if (view.IsCurrentAfterLast)
                view.MoveCurrentToFirst();
        }

        private void Add_Click_1(object sender, RoutedEventArgs e)
        {
            Family family = this.FindResource("family") as Family;
            family.Add(new Person("John", 55));
        }
        private void Sort_Click_1(object sender, RoutedEventArgs e)
        {
            Family family = this.FindResource("family") as Family;
            ICollectionView view = CollectionViewSource.GetDefaultView(family);

            if (view.SortDescriptions.Count == 0)
            {
                view.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
                view.SortDescriptions.Add(new SortDescription("Age", ListSortDirection.Descending));
            } 
            else {
                view.SortDescriptions.Clear();
            }
        }
        private void Filter_Click_1(object sender, RoutedEventArgs e)
        {
            Family family = this.FindResource("family") as Family;
            ICollectionView view = CollectionViewSource.GetDefaultView(family);

            if (view.Filter == null)
            {
                view.Filter = delegate(object item) { return ((Person)item).Age >= 40; };
            }
            else {
                view.Filter = null;
            }
        }

        private void Back_Click_1_XML(object sender, RoutedEventArgs e)
        {

            XmlDataProvider ds = (XmlDataProvider)this.FindResource("familyXml");
            IEnumerable family = (IEnumerable)ds.Data;
            ICollectionView view = CollectionViewSource.GetDefaultView(family);

            view.MoveCurrentToPrevious();
            if (view.IsCurrentBeforeFirst)
                view.MoveCurrentToLast();


        }
        private void Next_Click_1_XML(object sender, RoutedEventArgs e)
        {
            XmlDataProvider ds = (XmlDataProvider)this.FindResource("familyXml");
            IEnumerable family = (IEnumerable)ds.Data;
            ICollectionView view = CollectionViewSource.GetDefaultView(family);

            view.MoveCurrentToNext();
            if (view.IsCurrentAfterLast)
                view.MoveCurrentToFirst();
        }
        private void Add_Click_1_XML(object sender, RoutedEventArgs e)
        {
            XmlDataProvider ds = (XmlDataProvider)this.FindResource("familyXml");
            XmlDocument doc = ds.Document as XmlDocument;
            XmlElement person1 = doc.CreateElement("Person");
            person1.SetAttribute("Name", "Spartan");
            person1.SetAttribute("Age", "88");

            doc.ChildNodes[1].AppendChild(person1);

        }
        private void Sort_Click_1_XML(object sender, RoutedEventArgs e)
        {
            XmlDataProvider ds = (XmlDataProvider)this.FindResource("familyXml");
            IEnumerable family = (IEnumerable)ds.Data;
            ListCollectionView view = CollectionViewSource.GetDefaultView(family) as ListCollectionView;

            if ( view.CustomSort == null)
            {
                view.CustomSort = new PersonSorter();
            }
            else
            {
                view.CustomSort = null;
            }
        }
        private void Filter_Click_1_XML(object sender, RoutedEventArgs e)
        {
            XmlDataProvider ds = (XmlDataProvider)this.FindResource("familyXml");
            IEnumerable family = (IEnumerable)ds.Data;
            ICollectionView view = CollectionViewSource.GetDefaultView(family);

            if (view.Filter == null)
            {
                view.Filter = delegate(object item) { return int.Parse(((XmlElement)item).Attributes["Age"].Value) >= 40; };
            }
            else
            {
                view.Filter = null;
            }
            
        }

        private void Save_Click_1_XML(object sender, RoutedEventArgs args) {
            XmlDataProvider ds = (XmlDataProvider)this.FindResource("familyXml");
            ds.Document.Save("FamilyXML.xml");
            
        }
        
    }


    namespace DataM
    {
        public class Person : INotifyPropertyChanged
        {

            public event PropertyChangedEventHandler PropertyChanged;
            private void OnPropertyChanged(string propName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propName));
                }
            }

            public Person()
            {
                this._name = "john";
                this._age = 15;
            }

            int _age; string _name;
            public int Age { get { return _age; } set { _age = value; OnPropertyChanged("Age"); } }
            public string Name { get { return _name; } set { _name = value; OnPropertyChanged("Name"); } }
        }
    }
}
