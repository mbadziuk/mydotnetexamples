﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace TryWpf
{
    class NickName: INotifyPropertyChanged
    {
        string name;
        string nickName;
        
        public string Name { get { return name; } set { name = value; OnPropertyChanged("Name"); } }
        public string Nickname { get { return nickName; } set { nickName = value; OnPropertyChanged("Nickname"); } }
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string property) {
            if (PropertyChanged != null) {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public NickName(string _name, string _nick) {
            name = _name;
            nickName = _nick;
        }

   
    }

    class NickNames : ObservableCollection<NickName> { }
}
