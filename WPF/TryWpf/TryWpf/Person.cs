﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace TryWpf
{
    class Family : ObservableCollection<Person> { }

    class Person : INotifyPropertyChanged
    {
        int _age;
        string _name;

        public int Age
        {
            get { return _age; }
            set { _age = value; OnPropertyChanged("Age"); }
        }
        
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged("Name"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public Person(){}
        public Person(string p1, int p2)
        {
            // TODO: Complete member initialization
            this._name = p1;
            this._age = p2;
        }
        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }

    class PersonSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            XmlElement lhs = (XmlElement)x;
            XmlElement rhs = (XmlElement)y;
            // Sort Name ascending and Age descending
            int nameCompare = lhs.Attributes["Name"].Value.CompareTo(rhs.Attributes["Name"].Value);
            if (nameCompare != 0)
            {
                return nameCompare;
            }
            return int.Parse(rhs.Attributes["Age"].Value) - int.Parse(lhs.Attributes["Age"].Value);
        }
    }
}
