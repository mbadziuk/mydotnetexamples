﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DbWpfTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DbWpfTest.FriendsDataSetTableAdapters.FriendsDataTableAdapter friendsDataSetFriendsDataTableAdapter;

        DbWpfTest.FriendsDataSet friendsDataSet;

        public MainWindow()
        {
            InitializeComponent();

            ShowDataFromLocalDb();
        }

        private void ShowDataFromLocalDb()
        {
          

        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {

            friendsDataSet = ((DbWpfTest.FriendsDataSet)(this.FindResource("friendsDataSet")));
            // Load data into the table FriendsData. You can modify this code as needed.
            friendsDataSetFriendsDataTableAdapter = new DbWpfTest.FriendsDataSetTableAdapters.FriendsDataTableAdapter();
            friendsDataSetFriendsDataTableAdapter.Fill(friendsDataSet.FriendsData);
            System.Windows.Data.CollectionViewSource friendsDataViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("friendsDataViewSource")));
            friendsDataViewSource.View.MoveCurrentToFirst();
        }

        private void SaveFriend_Click(object sender, RoutedEventArgs e)
        {
            friendsDataSetFriendsDataTableAdapter.Update(friendsDataSet.FriendsData);
        }
    }
}
