﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SettingsWF
{
    public partial class Form1 : Form
    {
        private String mainSettings = "main.ini";
        private List<String> list = new List<string>();
        private String currentXmlToLoad;
        
        DataSet dataSet = new DataSet();
        private DataGridView selectedDgw;

        public Form1()
        {
            InitializeComponent();
        }

     


        private void LoadBtn_Click(object sender, EventArgs e)
        {
            dataSet.Clear();
            dataSet.ReadXml(currentXmlToLoad);
            DataTable dt = dataSet.Tables[0];
            dt.Columns[0].ReadOnly = true;

            DataGridView dgw = selectedDgw;
            dgw.DataSource = dataSet.Tables[0];
            dgw.AllowUserToAddRows = false;
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            dataSet.WriteXml(currentXmlToLoad, XmlWriteMode.WriteSchema);
        }

        private void OpenBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            openFileDialog1.ShowDialog();
        }

        #region | Init Logic |
        private void Form1_Load(object sender, EventArgs e)
        {
            OnStart();
        }

        private void OnStart()
        {
            list = LoadFilesToList();
            ListM.DataSource = list;
            InitXml();
            listBox1.DataSource = list;
        }

        private void InitXml()
        {
            foreach (string name in list)
            {
                if (!File.Exists(name))
                {
                    DataSet ds = new DataSet("Settings");
                    DataTable dt = new DataTable("PrivateConfigs");
                    DataColumn pkKeys = dt.Columns.Add("Keys", typeof(String));
                    dt.Columns.Add("Values", typeof(int));
                    ds.Tables.Add(dt);
                    dt.PrimaryKey = new DataColumn[] {pkKeys};

                    dt.Rows.Add(new[] {"Size", "0"});
                    dt.Rows.Add(new[] {"Height", "0"});
                    dt.Rows.Add(new[] {"Weight", "0"});

                    ds.WriteXml(name, XmlWriteMode.WriteSchema);
                }
            }
        }
        #endregion
        
        #region | ComboBox Logic |
        public List<String> LoadFilesToList()
        {
            List<String> result = new List<string>();
            StreamReader fileIn = new StreamReader(mainSettings);
            while (!fileIn.EndOfStream)
            {
                String line = fileIn.ReadLine();
                result.Add(line);
            }
            fileIn.Close();
            return result;
        }
        private void ListM_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentXmlToLoad = ListM.SelectedItem.ToString();
        }
        #endregion

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            currentXmlToLoad = openFileDialog1.FileName;
            LoadBtn_Click(sender, e);
        }

        private void CreateBtn_MouseClick(object sender, MouseEventArgs e)
        {
            saveFileDialog1.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            currentXmlToLoad = "\n" + Path.GetFileName(saveFileDialog1.FileName);
            
            File.AppendAllText(mainSettings, currentXmlToLoad);
            OnStart();
        }

        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            TabControl tabControl = (TabControl)sender;
            switch (tabControl.SelectedIndex)
            {
                case 0:selectedDgw = dataGridView1;
                    break;
                case 1:selectedDgw = dataGridView2;
                    break;
            }
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Really ?", "Delete file", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
            
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Sure", "Some Title", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                //do something
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Really ?", "Delete file", MessageBoxButtons.AbortRetryIgnore);
        }
    }
}
