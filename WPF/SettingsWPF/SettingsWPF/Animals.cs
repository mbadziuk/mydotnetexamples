﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SettingsWPF
{
    abstract class Animals
    {
        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        int _age;

        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }
        int _legs;

        public int Legs
        {
            get { return _legs; }
            set { _legs = value; }
        }
        int _weight;

        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public Animals() { }
        public Animals(string name, int age, int legs, int weight) 
        {
            Name = name;
            Age = age;
            Legs = legs;
            Weight = weight;
        }
    }

    class Cat : Animals 
    {
        string _color;

        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public Cat(string name, int age, int legs, int weight, string color) : base(name, age, legs, weight) {
            Color = color;
        }

    }
}
