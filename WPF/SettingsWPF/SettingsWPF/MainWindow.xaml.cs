﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Path = System.IO.Path;

namespace SettingsWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private String mainSettings = "main.ini";
        private List<String> list = new List<string>();
        private String currentXmlToLoad;

        DataSet dataSet = new DataSet();
        private DataGrid selectedDgw;

        List<Animals> MePetsList = new List<Animals>();

        public MainWindow()
        {
            InitializeComponent();

            //string name, int age, int legs, int weight, string color
            MePetsList.Add(new Cat("Kuzya", 2, 4, 7, "Grey"));
            MePetsList.Add(new Cat("Crippled", 6, 3, 8, "Black"));
            MePetsList.Add(new Cat("Monya", 10, 4, 8, "Green"));
            MePetsList.Add(new Cat("Vasya", 1, 4, 3, "Yellow"));

            Radius = 10;
            Step = 0.02;
            this.DataContext = this;
            AnimalsGrid.ItemsSource = MePetsList;
        }

        private void MainWindowApp_Loaded(object sender, RoutedEventArgs e)
        {
            OnStart();
        }

        private void OnStart()
        {
            list = LoadFilesToList();
            ListM.ItemsSource = list;
            InitXml();
            listBox1.ItemsSource = list;
            currentXmlToLoad = list[0];
        }

        public List<String> LoadFilesToList()
        {
            List<String> result = new List<string>();
            StreamReader fileIn = new StreamReader(mainSettings);
            while (!fileIn.EndOfStream)
            {
                String line = fileIn.ReadLine();
                result.Add(line);
            }
            fileIn.Close();
            return result;
        }

        private void InitXml()
        {
            foreach (string name in list)
            {
                if (!File.Exists(name))
                {
                    DataSet ds = new DataSet("Settings");
                    DataTable dt = new DataTable("PrivateConfigs");
                    DataColumn pkKeys = dt.Columns.Add("Keys", typeof(String));
                    dt.Columns.Add("Values", typeof(int));
                    ds.Tables.Add(dt);
                    dt.PrimaryKey = new DataColumn[] { pkKeys };

                    dt.Rows.Add(new[] { "Size", "0" });
                    dt.Rows.Add(new[] { "Height", "0" });
                    dt.Rows.Add(new[] { "Weight", "0" });

                    ds.WriteXml(name, XmlWriteMode.WriteSchema);
                }
            }
        }

        private void OpenBtn_Click(object sender, RoutedEventArgs e)
        {
            dataSet.Clear();
            dataSet.ReadXml(currentXmlToLoad);
            DataTable dt = dataSet.Tables[0];
            dt.Columns[0].ReadOnly = true;

            if (TabCtrl.SelectedIndex == 0)
            {
                selectedDgw = dataGrid1;
            }
            else
            {
                selectedDgw = dataGrid2;
            }
            
            DataGrid dg = selectedDgw;
            dg.ItemsSource = dataSet.Tables[0].DefaultView;
            MainInfoLbl.Content = "Settings in file => " + currentXmlToLoad;

            CreateDynamicButtons();
        }

        private void LoadBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (openFileDialog.ShowDialog() == true)
            {
                currentXmlToLoad = openFileDialog.FileName;
                OpenBtn_Click(sender, e);
            }
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currentXmlToLoad = listBox1.SelectedValue.ToString();
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            dataSet.WriteXml(currentXmlToLoad, XmlWriteMode.WriteSchema);
        }

        private void ListM_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ListM.SelectedValue != null)
                currentXmlToLoad = ListM.SelectedValue.ToString();
            else
                currentXmlToLoad = list[0];
        }

        private void CreateBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (sfd.ShowDialog() == true)
            {
                currentXmlToLoad = "\n" + Path.GetFileName(sfd.FileName);
                File.AppendAllText(mainSettings, currentXmlToLoad);
                OnStart();
            }
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            String fileToDelete;
            if (list.Contains(currentXmlToLoad))
            {
                fileToDelete = currentXmlToLoad;
                list.Remove(currentXmlToLoad);
                currentXmlToLoad = list[0];

                string contens = "";
                for (int i = 0; i < list.Count; i++)
                {
                    contens += list[i];

                    if (i != list.Count - 1)
                    {
                        contens += "\n";
                    }
                }
                File.Delete(mainSettings);
                File.WriteAllText(mainSettings, contens);

                int k = 0;
                foreach (Button btn in DynamicPanel.Children)
                {
                    if (btn.Content.ToString() == fileToDelete)
                        break;

                    k++;
                }
                DynamicPanel.Children.Remove(DynamicPanel.Children[k]);
            }
            OnStart();
        }

        private void QuitBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void AboutiMi_Checked(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure ?", "Quit dialogue", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }


        private void CreateDynamicButtons()
        {
            DynamicPanel.Children.Clear();

            int i = 0;
            foreach (string s in list)
            {
                String t;
                Button btn = new Button();
                t = s.Replace('.', '_');
                btn.Name = t + i;
                btn.Content = t;
                i++;
                btn.Click += OnDynamicButtonClick;
                DynamicPanel.Children.Add(btn);
            }
        }

        private void OnDynamicButtonClick(object sender, RoutedEventArgs e)
        {
            Button a = (Button) sender;
            string b = a.Content.ToString();
            string c = b.Replace('_','.');
            currentXmlToLoad = c;
            OpenBtn_Click(sender, e);
        }

        Point currentPosition;
        private Point circleCoords;
        public string RadiusS { get; set; }
        public string StepS { get; set; }
        public double Radius { get; set; }
        public double Step { get; set; }
      
        private void DrawDot()
        {
            Line l = new Line();
            l.Stroke = SystemColors.WindowFrameBrush;

            l.X1 = circleCoords.X;
            l.X2 = circleCoords.X + 1;
            l.Y1 = circleCoords.Y + 1;
            l.Y2 = circleCoords.Y;

            Canvas.Children.Add(l);
        }

        private void ClearCanvas(object sender, RoutedEventArgs e)
        {
            Canvas.Children.Clear();
        }

        private void TextBox_DataContextChanged_1(object sender, DependencyPropertyChangedEventArgs e)
        {
            
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void DrawPBtn_Click(object sender, RoutedEventArgs e)
        {
            if (polygonVertices.Count == 0 || polygonVertices.Count == 1)
                return;

            int finPointIndex = polygonVertices.Count - 1;

            for (int i = 0; i < polygonVertices.Count; i++) { 
                // calculate corner rate
                int currentDotIndex = i;
                int nextDotIndex = i + 1;

                if (i == finPointIndex) { 
                    currentDotIndex = i;
                    nextDotIndex = 0;
                }

                double deltaX = polygonVertices[nextDotIndex].X - polygonVertices[currentDotIndex].X;
                double deltaY = polygonVertices[nextDotIndex].Y - polygonVertices[currentDotIndex].Y;
                k = deltaY / deltaX;

                DrawLine(polygonVertices[currentDotIndex], polygonVertices[nextDotIndex]);
            }
        }

        private List<Point> polygonVertices = new List<Point>();
        private double k = 0;

        private void DrawPoligonCheckBox_Click(object sender, RoutedEventArgs e)
        {
            polygonVertices = new List<Point>();
            k = 0;
        }

        private void DrawLine(Point a, Point b)
        {
            double deltaX  = b.X - a.X;

            for ( ; a.X < b.X; a.X += Step )
            {
                circleCoords.X = a.X;
                circleCoords.Y = (a.X * k) + a.Y;
                DrawDot();
            }
        }

          private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            
            Point indicate = e.GetPosition(Canvas);

            XTb.Text = Convert.ToString(Math.Round(indicate.X,3));
            YTb.Text = Convert.ToString(Math.Round(indicate.Y,3));

            if (e.LeftButton == MouseButtonState.Pressed) {
                DrawCircle(indicate);
            }
            
        }

        private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
            {
                currentPosition = e.GetPosition(Canvas);
            }
            DrawCircle(currentPosition);
        }

        private void DrawCircle(Point a)
        {
            currentPosition.X = a.X;
            currentPosition.Y = a.Y;

            //for (double i = 0; i < 2 * Math.PI; i += Step)
            for (double i = 0; i < SliderSdr.Value; i += Step)
            {
                // calculate coords for x an y of circle
                circleCoords.X = currentPosition.X + Radius * Math.Cos(i);
                circleCoords.Y = currentPosition.Y + Radius * Math.Sin(i);
                DrawDot();
            }

            polygonVertices.Add(new Point(circleCoords.X, circleCoords.Y));
        
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {

        }


    }
}
