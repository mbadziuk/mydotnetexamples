// TrialDll.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "TrialDll.h"


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);


// Mine
HWND hWnd = NULL;
WNDPROC OldWndProc = NULL;
LRESULT CALLBACK	MyWndProc(HWND, UINT, WPARAM, LPARAM);

typedef enum  {
  DE_NoError = 0,       /**< Pas d'erreur */
  DE_InvalidParameter,  /**< Parametre invalide */
  DE_CommNotOpened,     /**< Port serie ferme */
  DE_Open,              /**< Erreur d'ouverture */
  DE_Write,             /**< Erreur d'ecriture */
  DE_Read,              /**< Erreur de lecture */
  DE_Config,            /**< Erreur de configuration */
  DE_UnknownEvent,      /**< Evenement inconnu */
  DE_Other,             /**< Erreur non determinee */
  DE_MissingDll,        /**< Librarie manquante */
  DE_BadSerialPortName, /**< Mauvais nom de port serie */
  DE_TaskNotStarted,    /**< Tache non demarree */
  DE_AlreadyOpened,     /**< Protocole deja ouvert */
  DE_NoData             /**< Pas de nouvelle donnees recues */
} T_Din66348Error;
typedef enum {
  DM_NORMAL = 0, /**< Utilise pour le BNA5 */
  DM_BSN3,       /**< Utilise par le BSN385 NT Service vers. 1.x */
  DM_BSN3EX,     /**< Utilise par le BSN385 NT Service vers. 2.x */
} T_DlMode;
typedef enum  {
  DP_LOW = 0,
  DP_MEDIUM,
  DP_HIGH,
} T_DLPriority;

typedef __declspec(dllimport) T_Din66348Error (__stdcall * T_MeiDin66348OpenPtr) (
  HWND client,
  int portNr,
  char * bnaCommLinkLibraryName,
  T_DlMode mode,
  T_DLPriority priorityLevel,
  bool * done 
  /*  -- backwords way
  bool * done, 
  T_DLPriority priorityLevel,
  T_DlMode mode,
  char * bnaCommLinkLibraryName,
  int portNr,
  HWND client
  */
  ); 

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TRIALDLL, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TRIALDLL));

	// �������� Callback ��� ��������� ��������� ����� Win32 ����������
	OldWndProc = (WNDPROC)GetWindowLongPtr(hWnd, GWLP_WNDPROC); // Cache QT WindowsMessageHandler
	SetWindowLongPtr(hWnd, GWL_WNDPROC, (LONG)MyWndProc); // Add our own WindowsMessageHandler

	
	// ��������� ����������� ����������
	HMODULE Din66348LibHandle = LoadLibrary(_T("C:\\Windows\\SysWOW64\\DIN66348export.dll"));
	T_MeiDin66348OpenPtr gs_MeiDin66348Open = NULL;

	if ( ! (Din66348LibHandle == NULL)) {
		gs_MeiDin66348Open = (T_MeiDin66348OpenPtr) GetProcAddress(Din66348LibHandle, "Open");
	}

	char comLib[] = "C:\Windows\SysWOW64\BNACommLink.dll";
	bool done = 0;

	gs_MeiDin66348Open( hWnd, 2, comLib, DM_NORMAL, DP_HIGH, &done);
	//gs_MeiDin66348Open( &done, DP_HIGH, DM_NORMAL, comLib, 2, hWnd ); -- backwords way
	
	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TRIALDLL));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_TRIALDLL);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK MyWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	return CallWindowProc(OldWndProc, hWnd, message, wParam, lParam);
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
