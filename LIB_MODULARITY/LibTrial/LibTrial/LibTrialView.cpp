
// LibTrialView.cpp : implementation of the CLibTrialView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "LibTrial.h"
#endif

#include "LibTrialDoc.h"
#include "LibTrialView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLibTrialView

IMPLEMENT_DYNCREATE(CLibTrialView, CView)

BEGIN_MESSAGE_MAP(CLibTrialView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CLibTrialView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// CLibTrialView construction/destruction

CLibTrialView::CLibTrialView()
{
	// TODO: add construction code here

}

CLibTrialView::~CLibTrialView()
{
}

BOOL CLibTrialView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CLibTrialView drawing

void CLibTrialView::OnDraw(CDC* /*pDC*/)
{
	CLibTrialDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CLibTrialView printing


void CLibTrialView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CLibTrialView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CLibTrialView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CLibTrialView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

void CLibTrialView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CLibTrialView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CLibTrialView diagnostics

#ifdef _DEBUG
void CLibTrialView::AssertValid() const
{
	CView::AssertValid();
}

void CLibTrialView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CLibTrialDoc* CLibTrialView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLibTrialDoc)));
	return (CLibTrialDoc*)m_pDocument;
}
#endif //_DEBUG


// CLibTrialView message handlers
