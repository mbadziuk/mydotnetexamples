﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;

namespace MyLibrary
{
    /** 
     * \class AsyncWorker
     * \brief Simple async worker.
     * This worker runs in background, reports progress and complete result, and can be safely cancelled
     */
    public class AsyncWorker : BackgroundWorker
    {
        /** 
         * \brief Progress count.
         * Field shows the progress of backgroundworker. Don't forget to increment it.
         */
        int percentReady = 0;

        /** 
         * \brief Finished bool.
         * Shows if this worker finished it's work or not -> true when percentReady == 100
         */
        public bool Finished { get { return percentReady == 100; } }

        /** 
         * \brief Result of worker here.
         * From this field you can retrive result of worker at the end
         */
        private string ResultAccumulator;

        /** 
         * \brief Args for worker.
         * Args for worker which client send to worker can be of any type
         */
        private string _request;

        /** A constructor.
         * @param progressHandler receives a delegate to a callback method for reporting progress of this worker
         * @param completeHandler receives a delegate to a callback method for getting Result on Worker complete work
         */
        public AsyncWorker(ProgressChangedEventHandler progressHandler, RunWorkerCompletedEventHandler completeHandler)
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;

            ProgressChanged += progressHandler;
            RunWorkerCompleted += completeHandler;
        }

        /**
         * @code 
         *  void main()
         * {
         *    AsyncWorker bg_worker = new AsyncWorker(bg_worker_Progress, bg_worker_Complete);
         *   bg_worker.RunWorkerAsync("www.google.com");
         *
         *    Console.ReadKey();
         *
         *   bg_worker.CancelIt();
         *
         *    Console.ReadKey();
         *
         * }
         *   private static void bg_worker_Complete(object sender, RunWorkerCompletedEventArgs e)
         * {
         *      if (e.Cancelled)
         *          Console.WriteLine("User Cancelled Thread !!");
         *      else if (e.Error != null)
         *          Console.WriteLine("Worker Exception => {0}", e.Error);
         *      else
         *          Console.WriteLine(e.Result);
         *  }
         *  private static void bg_worker_Progress(object sender, ProgressChangedEventArgs e)
         *  {
         *      Console.WriteLine(" {0}% progress... ", e.ProgressPercentage );
         *  }
         * @endcode
         */
        protected override void OnDoWork(DoWorkEventArgs e)
        {
            _request = e.Argument as String;
            ResultAccumulator += _request;

            ReportProgress(percentReady, "Getting content from remote source...");

            while (!Finished)
            {
                ResultAccumulator += "A-";

                if (IsThreadCancelled(e))
                    return;

                ReportProgress(++percentReady, " %  downloading...");
                Thread.Sleep(100);
            }
            ReportProgress(percentReady, "% Download Complete !!!");

            e.Result = ResultAccumulator;
        }

        private bool IsThreadCancelled(DoWorkEventArgs e)
        {
            if (CancellationPending)
            {
                e.Cancel = true;
                FreeUnmanagedResources();
                return true;
            }
            return false;
        }

        private void FreeUnmanagedResources()
        {
            Console.WriteLine("Free resources !!! ");
        }

        /** 
         * \brief Cancel worker.
         * A method will safely stop worker and free it's resources
         */
        public void CancelIt()
        {
            if (IsBusy)
            {
                CancelAsync();
            }
        }

     
    }
   
    public class Spinlock
    {
        private static int _block;

        public void AcquireResourse()
        {
            int unlocked = 1;
            bool gotResource = false;

            while (unlocked == 1 && !gotResource)
            {
                unlocked = Interlocked.CompareExchange(ref _block, 1, 0);

                if (unlocked == 0)
                    gotResource = true;
                else
                    Thread.Sleep(10);
            }
        }

        public void ReleaseResource()
        {
            Interlocked.Exchange(ref _block, 0);
        }
    }


}
