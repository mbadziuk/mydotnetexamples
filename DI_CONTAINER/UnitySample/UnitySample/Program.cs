﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Microsoft.Win32;


namespace UnitySample
{
    class Program
    {
        interface ILogger { 
            void Write(string msg);
        }
        
        class FileLogger : ILogger {
            public void Write(string msg) {
                System.Diagnostics.Debug.WriteLine("\n\nI am FileLogger and logging to a file => " + msg);
            }
        }
        class SmsLogger : ILogger
        {
            public void Write(string msg)
            {
                System.Diagnostics.Debug.WriteLine("\n\nI am SmsLogger and logging to a SMS=> " + msg);
            }
        }
        class EmailLogger : ILogger
        {
            public void Write(string msg)
            {
                System.Diagnostics.Debug.WriteLine("\n\nI am EmailLogger and logging to a email => " + msg);
            }
        }


        class Logger {
            ILogger logger;

            ILogger logger2;
            [Dependency("file")]
            public ILogger _Logger2 { get { return logger2; } set { logger2 = value; } }

            public ILogger _Logger { get { return logger; } set { logger = value; } }

            public Logger([Dependency("test")]ILogger _logger)
            {
                _Logger = _logger;
            }

            
            
            public void Write(string msg)
            {
                _Logger.Write(msg);
            }
        }

        static void Main(string[] args)
        {
            IUnityContainer container = new UnityContainer();
            RegisterTypesInContainer(container);

            ILogger logger = container.Resolve<ILogger>("file");
            logger.Write("Maksym message! ");


            logger = container.Resolve<ILogger>("email");
            logger.Write("Maksym message! ");

            Logger _logger = container.Resolve<Logger>("test");
            _logger.Write("Maksym message! ");

            Console.ReadKey();

        }

        private static void UnityContainerSample()
        {
            IUnityContainer container = new UnityContainer();
            RegisterTypesInContainer(container);

            ILogger logger = container.Resolve<ILogger>("file");
            logger.Write("Maksym message! ");


            logger = container.Resolve<ILogger>("email");
            logger.Write("Maksym message! ");

            Logger _logger = container.Resolve<Logger>("test");
            _logger.Write("Maksym message! ");
        }

        public static void RegisterTypesInContainer(IUnityContainer container)
        {

            container.RegisterType<ILogger, SmsLogger>();
            container.RegisterType<ILogger, FileLogger>("file");
            container.RegisterType<ILogger, EmailLogger>("email");
            container.RegisterType<ILogger, SmsLogger>("sms");
            container.RegisterType<ILogger, SmsLogger>("test");
            //container.RegisterType<Logger>();
        }
    }
}
