// MyTestBnr.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

/************************ Defines and typedefs *******************************/
#define BNR_CASHTAKEN_TIME_OUT_IN_MS            (30000)
#define BNR_DEFAULT_OPERATION_TIME_OUT_IN_MS    (5000)
#define BNR_OPEN_OPERATION_TIME_OUT_IN_MS       (1000)
#define BNR_RESET_OPERATION_TIME_OUT_IN_MS      (60000)
#define BNR_CASHIN_OPERATION_TIME_OUT_IN_MS     (30000)


static HANDLE signal;
static T_XfsCashOrder cashOrder;
static T_BnrXfsResult result;

static void __stdcall operationComplete(
	LONG32 identificationId,
	LONG32 operationId,
	LONG32 result,
	LONG32 extendedResult, 
	void *data);

static void __stdcall operationComplete(LONG32 identificationId, LONG32 operationId, LONG32 result, LONG32 extendedResult, void *data)
{
	switch(operationId)
	{
		case XFS_O_CDR_CASH_IN: 
			if (data != NULL)
				memcpy(&cashOrder, data, sizeof(T_XfsCashOrder));
			break;
	}
	SetEvent(signal);
}

static void __stdcall statusOccured(
  LONG32 status, LONG32 result, LONG32 extendedResult, void *data)
{
}
static void __stdcall intermediateOccured(
  LONG32 identificationId, LONG32 operationId, LONG32 reason, void *data)
{
}//intermediateOccured

typedef enum 
{
	Open,
	CashinStart,
	Cashin,
	CashinEnd,
	Close
} Operations;

static T_BnrXfsResult Command(Operations opId)
{
	int timeWait = 0;
	ResetEvent(signal);
	switch (opId)
	{
		case Open: result = bnr_Open(operationComplete, statusOccured, intermediateOccured); timeWait = BNR_OPEN_OPERATION_TIME_OUT_IN_MS; break;
		case CashinStart: result = bnr_CashInStart();
		case Cashin: result =
		case CashinEnd: result =
		case Close: result =
	}
	if (result > BXR_NO_ERROR)
		WaitForSingleObject(signal, timeWait);

	return result;
}

static int makeBnrOperational()
{
	T_XfsCdrStatus bnrStatus;

	Command(Open);

	if (bnr_GetStatus(&bnrStatus) == BXR_NO_ERROR) 
	{
		if (bnrStatus.deviceStatus != XFS_S_CDR_DS_ON_LINE) 
		{
			ResetEvent(signal);
			bnr_Reset();
			WaitForSingleObject(signal, BNR_RESET_OPERATION_TIME_OUT_IN_MS);

			return 0;
		}
    }
	return -1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int insertSumm;
	int insertedSumm = 0;

	signal = CreateEvent(NULL, TRUE, FALSE, NULL);
	
	printf("Set summ to accept in MDU: ");
	scanf_s("%d", &insertSumm);
	printf("\n");

	if (makeBnrOperational == 0)
	{
		Command(CashinStart);

		while(insertedSumm < insertedSumm)
		{
			Command(Cashin);
		}
		
	}




	return 0;
}



