package com.mei.uniontest;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Structure;
import com.sun.jna.Union;

public class CLibraryAdapter {
	
	public interface CLibraryInterface extends Library {
		
		public static final String JNA_LIBRARY_NAME = "TestUnionCLibrary";
		
		public static final CLibraryInterface INSTANCE =
			(CLibraryInterface) Native.loadLibrary(JNA_LIBRARY_NAME, CLibraryInterface.class);

		public static final CLibraryInterface SYNC_INSTANCE =
			(CLibraryInterface) Native.synchronizedLibrary(INSTANCE);
		
		public static final int BUILD_LENGTH = (int)(5);
		public static final int MAX_NR_OF_CASH_TYPE_HISTORY = (int)(61);
		
		public class CashUnitAdapter extends Structure {
			
			public static class ByReference extends CashUnitAdapter implements Structure.ByReference {}
			
			public NativeLong transportCount;
			public LogicalCashUnitAdapter logicalCashUnit;
			
			@Override
			protected List<?> getFieldOrder() {
				
				return Arrays.asList("transportCount", "logicalCashUnit");
			}
			
		}
		public class LogicalCashUnitAdapter extends Structure {
			
			public static class ByReference extends LogicalCashUnitAdapter implements Structure.ByReference {}
			
			public NativeLong count;
			public byte[] unitId = new byte[5];
			
			@Override
			protected List<?> getFieldOrder() {
				return Arrays.asList("count", "unitId");
			}

		}
		
		public class ExtendedCountersAdapter extends Union {
		
			public static class ByReference extends ExtendedCountersAdapter implements com.sun.jna.Structure.ByReference {};
			
			public DepositCountersAdapter deposit;
			public DispenseCountersAdapter dispense;
			
			public void read() {
				setType(CLibraryInterface.DepositCountersAdapter.class);
		        super.read();
		    }
			
		}
		
		public class DepositCountersAdapter extends Structure {
			
			public static class ByReference extends DepositCountersAdapter implements Structure.ByReference {}
			
			public NativeLong depositCount;
			public NativeLong retractedCount;

			protected List<?> getFieldOrder() {
				return Arrays.asList("depositCount", "retractedCount");
			}
		}

		public class DispenseCountersAdapter extends Structure {
			
			public static class ByReference extends DispenseCountersAdapter implements Structure.ByReference {}
			
			public NativeLong dispenseCount;
			public NativeLong rejectCount;
			
			protected List<?> getFieldOrder() {
				return Arrays.asList("dispenseCount", "rejectCount");
			}
		}

		void bnr_displayHelloFromDLL();
		void bnr_getCashUnit(CashUnitAdapter.ByReference cua);
		void bnr_getCountersUnion(ExtendedCountersAdapter.ByReference eca);
		
	}

	public void displayHelloFromDLL() {
		
		CLibraryInterface.SYNC_INSTANCE.bnr_displayHelloFromDLL();
		
	}
	
	public void getCashUnit() {

		CLibraryInterface.CashUnitAdapter.ByReference cua = new CLibraryInterface.CashUnitAdapter.ByReference();
		
		CLibraryInterface.SYNC_INSTANCE.bnr_getCashUnit(cua);
		
		String lcuName = new String(cua.logicalCashUnit.unitId);
		System.out.println("Logical Cash Unit name = " + lcuName.trim());
	} 

	public void getCountersUnion() {
		
		CLibraryInterface.ExtendedCountersAdapter.ByReference eca = new CLibraryInterface.ExtendedCountersAdapter.ByReference();	

		CLibraryInterface.SYNC_INSTANCE.bnr_getCountersUnion(eca);
		
		System.out.println("DepositCount = " + eca.deposit.depositCount);
		System.out.println("DepositCount = " + eca.deposit.retractedCount);
	
	} 
}
