#include "TestUnionCLib.h"

EXTERN_C UNION_DLL_API void bnr_displayHelloFromDLL()
  {
    printf ("Hello from DLL !\n");
  }

EXTERN_C UNION_DLL_API void bnr_getSimpleUnion()
  {

  }

EXTERN_C UNION_DLL_API void bnr_getCashUnit(CashUnit* cashUnit) {

	cashUnit->transportCount = 999;

	cashUnit->logicalCashUnit.count = 666;
	
	cashUnit->logicalCashUnit.unitId[0] = 'M';
	cashUnit->logicalCashUnit.unitId[1] = 'a';
	cashUnit->logicalCashUnit.unitId[2] = 'x';
	cashUnit->logicalCashUnit.unitId[3] = 'S';
	cashUnit->logicalCashUnit.unitId[4] = '\0';
}

EXTERN_C UNION_DLL_API void bnr_getCountersUnion(ExtendedCounters* extCtrs) {
	extCtrs->deposit.depositCount = 111;
	extCtrs->deposit.retractCount = 222;
	printf("CLibOutput ");

	printf("CLibOutput => depositCounter = %d\n",extCtrs->deposit.depositCount );
	printf("CLibOutput => retractCount = %d\n",extCtrs->deposit.retractCount );
}