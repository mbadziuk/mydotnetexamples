#ifndef _TEST_UNION_C_LIB_H_
#define _TEST_UNION_C_LIB_H_

#define CALLTYPE __stdcall
#define UNION_DLL_API __declspec(dllexport)
#define EXTERN_C extern "C"
#include <stdio.h>

typedef char UnitId[5];

typedef struct LogicalCashUnit {
	int count;
	UnitId unitId;   // char[]
};

typedef struct CashUnit {
	int transportCount;
	LogicalCashUnit logicalCashUnit;
};

typedef struct DepositCounters {
	int depositCount;
	int retractCount;
};

typedef struct DispenseCounters {
	int dispenseCount;
	int rejectCount;
};

typedef union ExtendedCounters {
	DepositCounters deposit;
	DispenseCounters dispense;
};


EXTERN_C UNION_DLL_API   void bnr_displayHelloFromDLL();
EXTERN_C UNION_DLL_API   void bnr_getSimpleUnion();
EXTERN_C UNION_DLL_API void bnr_getCashUnit(CashUnit* logicalCashUnit);
EXTERN_C UNION_DLL_API void bnr_getCountersUnion(ExtendedCounters* extCtrs);

#endif _TEST_UNION_C_LIB_H_
