namespace Movie.Migrations
{
    using Movie.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Movie.Models.MoviemDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Movie.Models.MoviemDBContext context)
        {
            context.Movies.AddOrUpdate(i => i.Title,
             new Moviem
             {
                 Title = "When Harry Met Sally",
                 ReleaseDate = DateTime.Parse("1989-1-11"),
                 Genre = "Romantic Comedy",
                 Rating = "G",
                 Price = 7.99M

             },

              new Moviem
              {
                  Title = "Ghostbusters ",
                  ReleaseDate = DateTime.Parse("1984-3-13"),
                  Genre = "Comedy",
                  Rating = "G",
                  Price = 8.99M
              },

              new Moviem
              {
                  Title = "Ghostbusters 2",
                  ReleaseDate = DateTime.Parse("1986-2-23"),
                  Genre = "Comedy",
                  Rating = "G",
                  Price = 9.99M
              },

            new Moviem
            {
                Title = "Rio Bravo",
                ReleaseDate = DateTime.Parse("1959-4-15"),
                Genre = "Western",
                Rating = "G",
                Price = 3.99M
            }
        );
        }
    }
}
