namespace Movie.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataAnnotationsMig : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Moviems", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Moviems", "Genre", c => c.String(nullable: false));
            AlterColumn("dbo.Moviems", "Rating", c => c.String(maxLength: 5));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Moviems", "Rating", c => c.String());
            AlterColumn("dbo.Moviems", "Genre", c => c.String());
            AlterColumn("dbo.Moviems", "Title", c => c.String());
        }
    }
}
