namespace Movie.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRatingMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Moviems", "Rating", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Moviems", "Rating");
        }
    }
}
