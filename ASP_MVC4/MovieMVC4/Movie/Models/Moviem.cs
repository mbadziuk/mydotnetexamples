﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Movie.Models
{
    public class Moviem
    {
        public int ID { get; set; }

        [Required]
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        [Required]
        public string Genre { get; set; }

        [Range(1,100)]
        public decimal Price { get; set; }

        [StringLength(5)]
        [DataType(DataType.Currency)]
        public string Rating { get; set; }
    }

    public class MoviemDBContext : DbContext
    {
        public DbSet<Moviem> Movies { get; set; }
    }
}