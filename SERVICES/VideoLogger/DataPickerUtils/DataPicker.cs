﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Timers;
using System.Threading;

namespace DataPickerUtils
{
    public class DataPicker
    {
        #region | Fields |

        private string _observedServiceDir;

        private bool _isStoped = false;

        private string _videoLoggerSourceDir;
        private string _videoLoggerDestinationDir = @"BnaVideoLogs";
       
        private string _videoLoggerDestinationDevicePrefixDir = @"Device_";
        private string _deviceNumber;
        private string _deviceLocation;
        private string _dataPickFileExtension;

        private int BnrDefaultOperationTimeOutInMS = 10000;
        private bool _isVideoLogsPicked = false;
        private System.Timers.Timer newTimer;

        // Flash Volume Name
        private string _volumeLabelForVideoLogs = @"VIDEO_LOGS_REPORT";

        private BackgroundWorker _bgwFlashMonitor = new BackgroundWorker();
        #endregion

        public DataPicker()
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            GetVideoLogsConfigurations();
            GetBnaConfigurations();

            _bgwFlashMonitor.DoWork += LookForRemovableDevice;

            Console.WriteLine("");
        }

        #region | Configurations |
        private void GetBnaConfigurations()
        {
            GetObservedServiceDir();
            GetDevicePlaceAndNumber();
        }

        private void GetVideoLogsConfigurations()
        {
            _videoLoggerSourceDir = @"C:\videologs\video\videologs";
            _dataPickFileExtension = @"mp4";

            DataSet ds = new DataSet();
            ds.ReadXml(System.AppDomain.CurrentDomain.BaseDirectory + @"\videoLogger.ini");

            DataTable dt = ds.Tables["Settings"];

            // VideoLogDirectory
            DataRow[] drs = dt.Select("key = 'VideoLogDirectory'");
            if (drs.Length > 0)
            {
                _videoLoggerSourceDir = Convert.ToString(drs[0]["value"]);
            }

            // VolumeLabelForVideoLogs
            drs = dt.Select("key = 'VolumeLabelForVideoLogs'");
            if (drs.Length > 0)
            {
                _volumeLabelForVideoLogs = Convert.ToString(drs[0]["value"]);
            }

            // DataPickFileExtenstion
            drs = dt.Select("key = 'DataPickFileExtenstion'");
            if (drs.Length > 0)
            {
                _dataPickFileExtension = Convert.ToString(drs[0]["value"]);
            }
        }

        private void GetDevicePlaceAndNumber()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(_observedServiceDir + @"\bnrCash.ini");

            DataTable dt = ds.Tables["Setings"];
            DataRow[] drs = dt.Select("key = 'PointOfSaleName'");

            if (drs.Length > 0)
            {
                _deviceLocation = Convert.ToString(drs[0]["value"]);
            }

            drs = dt.Select("key = 'BNA_BNR_Device_Number'");
            if (drs.Length > 0)
            {
                _deviceNumber = Convert.ToString(drs[0]["value"]);
            }
        }

        private void GetObservedServiceDir()
        {
            String path;

            using (RegistryKey regKeyLocalM = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\services\BnrCashService"))
            {
                Object directoryObject = regKeyLocalM.GetValue("ImagePath");
                path = Path.GetDirectoryName(directoryObject as String);
                regKeyLocalM.Close();
            }
            //HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\ + ServiceName  ==> Registry names for Services

            _observedServiceDir = path;
        }
        #endregion

        #region | DataPicker |
             
        public void Start()
        {
            _isStoped = false;
            _bgwFlashMonitor.RunWorkerAsync();
        }

        public void Stop()
        {
            _isStoped = true;

            //while (_bgwFlashMonitor.IsBusy)
            //{
            //    System.Threading.Thread.Sleep(20);
            //}
        }

        #region | Background Worker for Copying Reports to Engineer Removable Flash Drive |

        private void LookForRemovableDevice(object sender, DoWorkEventArgs eventArgs)
        {
            while (!_isStoped)
            {
                // Console.Write(".");
                Thread.Sleep(1000);
                try
                {
                    FileInfo[] allVideoFiles = GetAllVideos();

                    if (allVideoFiles.Length == 0)
                        continue;

                    DriveInfo[] drives = DriveInfo.GetDrives();
                    DriveInfo drive = IsUsbForLogsMounted(drives);
                        
                    if ( drive != null ) 
                    {
                        string destinationDataPath = drive.Name + _videoLoggerDestinationDir
                                                                + "\\" + _videoLoggerDestinationDevicePrefixDir + _deviceLocation + "_" + _deviceNumber
                                                                + "\\" + DateTime.Now.Date.DayOfYear.ToString();

                        if (!Directory.Exists(destinationDataPath))
                            Directory.CreateDirectory(destinationDataPath);

                        // Move Data
                        foreach (FileInfo file in allVideoFiles)
                        {
                            MoveData(file, destinationDataPath);
                        }

                        ClearAllAdditionalData("\\thumbs");
                        ClearAllAdditionalData("\\grabs");
                        ClearAllAdditionalData("");

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error -> during moving video files.");
                }
            }
        }

        private void ClearAllAdditionalData(string pathToClear)
        {
            DirectoryInfo dir = new DirectoryInfo(_videoLoggerSourceDir + pathToClear);

            if (dir != null)
            {
                FileInfo[] files = dir.GetFiles();

                foreach (FileInfo file in files)
                {
                    file.Delete();
                }
            }
        }

        private FileInfo[] GetAllVideos()
        {
            DirectoryInfo dirVideoLogs = new DirectoryInfo(_videoLoggerSourceDir);
            FileInfo[] videoLogFiles = dirVideoLogs.GetFiles("*." + _dataPickFileExtension);

            return videoLogFiles;
        }

        private DriveInfo IsUsbForLogsMounted(DriveInfo[] drivesInfo)
        {
          DriveInfo Return = null;

          List<DriveInfo> drives = drivesInfo.Where<DriveInfo>( driveInfo => driveInfo.DriveType == DriveType.Removable &&
                                                                             driveInfo.VolumeLabel == _volumeLabelForVideoLogs ).ToList<DriveInfo>();
          if (drives.Count != 0)
          {
              Return = drives[0];
          }

          return Return;
        }

        private void MoveData(FileInfo fileInfo, string path)
        {
            try
            {
                lock (fileInfo)
                {
                    fileInfo.MoveTo(path + "\\" + fileInfo.Name);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Video file Locked exception !");
            }
        }
        #endregion

        #endregion
       
    }

}