﻿using DataPickerUtils;
using Microsoft.Win32;
using ProgramStarterUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace BnaVideoLogger
{
    public partial class Service1 : ServiceBase
    {
        DataPicker dataPicker;
        ProgramStarter programStarter;
        
        public Service1()
        {
            InitializeComponent();

            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            dataPicker = new DataPicker();
        }

        protected override void OnStart(string[] args)
        {
            dataPicker.Start();
        }

        protected override void OnStop()
        {
            dataPicker.Stop();
        }
    }
}
