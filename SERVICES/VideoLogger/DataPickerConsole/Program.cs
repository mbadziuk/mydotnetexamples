﻿using DataPickerUtils;
using ProgramStarterUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataPickerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DataPicker dp = new DataPicker();
            ProgramStarter ps = new ProgramStarter(LockPcType.SwitchUser);

            ps.Start();
            dp.Start();

            //Console.ReadKey();
            LockWorkStationSafe();
            Console.ReadKey();

            ps.Stop();
            dp.Stop();

            

            
        }

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern bool LockWorkStation();

        public static void LockWorkStationSafe()
        {
            bool result = LockWorkStation();

            if (result == false)
            {
                // An error occured
                throw new System.ComponentModel.Win32Exception(System.Runtime.InteropServices.Marshal.GetLastWin32Error());
            }
        }
    }
      

}
