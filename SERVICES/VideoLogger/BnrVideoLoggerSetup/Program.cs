﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BnrVideoLoggerSetup
{
    class Program
    {
        private static string _appPath;

        static void Main(string[] args)
        {
            GetConfigurations();
            _appPath = "\"" + _appPath + "\"";


            try
            {
                if (args[0] == "1")
                    AddAppToStartUpWin();
                else
                    DeleteAppFromStartUpWin();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static void AddAppToStartUpWin()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true))
            {
                key.SetValue("BnrVideoLoggerStartup", _appPath);
            }
        }

        private static void DeleteAppFromStartUpWin()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true))
            {
                key.DeleteValue("BnrVideoLoggerStartup", false);
            }
        }

        private static void GetConfigurations()
        {
            _appPath = @"C:\BnrVideoLogger\BnrVideoLoggerApp.exe";

            DataSet ds = new DataSet();
            ds.ReadXml(System.AppDomain.CurrentDomain.BaseDirectory + @"\videoLogger.ini");

            DataTable dt = ds.Tables["Settings"];
            DataRow[] drs = dt.Select("key = 'BnrVideoLoggerAppFullPath'");

            if (drs.Length > 0)
            {
                _appPath = Convert.ToString(drs[0]["value"]);
            }
        }
    }
}
