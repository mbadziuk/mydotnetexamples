﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ProgramStarterUtils
{
    public enum LockPcType {
        LockComputer,
        SwitchUser
    }

    public class ProgramStarter
    {
        private string _spyCameraAppFullPath;
        private Process _app;
        private Process _appMonitor;
        
        public ProgramStarter(LockPcType lockPcType)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            GetConfigurations();
            PrepareProcess();
            Start();

            switch (lockPcType) {
                case LockPcType.LockComputer: LockWorkStationSafe(); break;
                case LockPcType.SwitchUser: SwitchUserSafe(); break;
                default: SwitchUserSafe(); break;
            }
        }

        public void Start()
        {
            if (!Process.GetProcesses().Any(p => p.ProcessName == "iSpy"))
            {
                _app.Start();
                Console.WriteLine("iSpy Camera Software Started.");
            }
        }
        public void Stop() 
        {
            // should kill monitor first
            _appMonitor = Process.GetProcesses().Where(p => p.ProcessName == "iSpyMonitor").ToList<Process>()[0];
            _appMonitor.Kill();
            _app.Kill();
            Console.WriteLine("iSpy Camera Software Stopped.");
        }

        private void PrepareProcess()
        {
            Process[] processs = Process.GetProcesses();

            if (!processs.Any(p => p.ProcessName == "iSpy"))
            {
                _app = new Process();
                _app.StartInfo.FileName = _spyCameraAppFullPath;
            }
            else
            {
                _app = Process.GetProcesses().Where(p => p.ProcessName == "iSpy").ToList<Process>()[0];
            }
        }

        private void GetConfigurations()
        {
            string _spyCameraAppFullPath_win32 = @"C:\Program Files\iSpy\iSpy\iSpy.exe"; // win32
            string _spyCameraAppFullPath_win32_1 = @"C:\ProgramFiles\iSpy\iSpy\iSpy.exe"; // win32

            _spyCameraAppFullPath = @"C:\Program Files (x86)\iSpy\iSpy\iSpy.exe"; // win64
            
            DataSet ds = new DataSet();
            ds.ReadXml(System.AppDomain.CurrentDomain.BaseDirectory + @"\videoLogger.ini");

            DataTable dt = ds.Tables["Settings"];
            DataRow[] drs = dt.Select("key = 'SpyCameraAppFullPath'");

            if (drs.Length > 0)
            {
                _spyCameraAppFullPath = Convert.ToString(drs[0]["value"]);
            }

            FileInfo file64 = new FileInfo(_spyCameraAppFullPath);
            FileInfo file32 = new FileInfo(_spyCameraAppFullPath_win32);
            FileInfo file321 = new FileInfo(_spyCameraAppFullPath_win32_1);

            if ( file64.Exists ) {
                return;
            }
            else if (file32.Exists) {
                _spyCameraAppFullPath = _spyCameraAppFullPath_win32;
            }
            else if (file321.Exists)
            {
                _spyCameraAppFullPath = _spyCameraAppFullPath_win32_1;
            }
            else {
                Console.WriteLine("Please Correct ISpy installation dir in videoLogger.ini file");
                throw new Exception();
            }
        }

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern bool LockWorkStation();

        public static void LockWorkStationSafe()
        {
            bool result = LockWorkStation();

            if (result == false)
            {
                // An error occured
                throw new System.ComponentModel.Win32Exception(System.Runtime.InteropServices.Marshal.GetLastWin32Error());
            }
        }



        [System.Runtime.InteropServices.DllImport("wtsapi32.dll", SetLastError = true)]
        static extern bool WTSDisconnectSession(IntPtr hServer, int sessionId, bool bWait);

        const int WTS_CURRENT_SESSION = -1;
        static readonly IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;

        public static void SwitchUserSafe() 
        {
            if (!WTSDisconnectSession(WTS_CURRENT_SERVER_HANDLE, WTS_CURRENT_SESSION, false))
                throw new System.ComponentModel.Win32Exception();
        }
    }
}
