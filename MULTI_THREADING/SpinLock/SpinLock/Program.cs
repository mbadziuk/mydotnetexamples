﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace SpinLock
{
  class Program
    {
        static FileStream _sharedStream = new FileStream("sharedFile.txt", FileMode.Append, FileAccess.Write);

        public static void Worker()
        {
            int id = Thread.CurrentThread.ManagedThreadId;
            Spinlock spin = new Spinlock();
            int count = 1;
            TextWriter writer;
            

            while (count < 150)
            {
                spin.AcquireResourse();

                writer = new StreamWriter(_sharedStream);
                writer.WriteLine("Thread {0} - count {1}", id, count++);
                writer.Flush();

                spin.ReleaseResource();
            }
        }

        static void Main(string[] args)
        {
            Thread[] threads = new Thread[150];

            // Initialize and Start all Threads
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(Worker);
                threads[i].Start();
            }

            // Wait in main thread for all threads to finish
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
            }

            _sharedStream.Close();

            Console.WriteLine("Writing finished... Press any key...");
            Console.ReadKey();
        }
    }
}
