﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;


namespace SpinLock
{
    class Spinlock
    {
        private static int _block;

        public void AcquireResourse()
        {
            int unlocked = 1;
            bool gotResource = false;

            while (unlocked == 1 && !gotResource)
            {
                unlocked = Interlocked.CompareExchange(ref _block, 1, 0);

                if (unlocked == 0)
                    gotResource = true;
                else
                    Thread.Sleep(10);
            }
        }

        public void ReleaseResource()
        {
            Interlocked.Exchange(ref _block, 0);
        }
    }

}
