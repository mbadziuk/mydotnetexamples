﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using BackgroundWork;

namespace BackgroundWork
{
    class AsyncWorker : BackgroundWorker
    {
        int percentReady = 0;
        public bool Finished {get { return percentReady == 100; } }
        private string ResultAccumulator;
        private string _request;
        
        public AsyncWorker(ProgressChangedEventHandler progressHandler, RunWorkerCompletedEventHandler completeHandler)
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;

            ProgressChanged += progressHandler;
            RunWorkerCompleted += completeHandler;
        }

        protected override void OnDoWork(DoWorkEventArgs e)
        {
            _request = e.Argument as String;
            ResultAccumulator += _request;

            ReportProgress(percentReady, "Getting content from remote source...");

            while ( ! Finished )
            {
                ResultAccumulator += "A-";

                if (IsThreadCancelled(e)) 
                    return;

                ReportProgress(++percentReady," %  downloading...");
                Thread.Sleep(100);
            }
            ReportProgress(percentReady, "% Download Complete !!!");

            e.Result = ResultAccumulator;
        }

        private bool IsThreadCancelled(DoWorkEventArgs e)
        {
            if (CancellationPending)
            {
                e.Cancel = true;
                FreeUnmanagedResources();
                return true;
            }
            return false;
        }

        private void FreeUnmanagedResources()
        {
            Console.WriteLine("Free resources !!! ");
        }

        public void CancelIt()
        {
            if (IsBusy)
            {
                CancelAsync();
            }
        }
    }
    
       
    internal class Program
    {
        private static void Main(string[] args)
        {
            AsyncWorker bg_worker = new AsyncWorker(bg_worker_Progress, bg_worker_Complete);
            bg_worker.RunWorkerAsync("www.google.com");

            Console.ReadKey();

            bg_worker.CancelIt();

            Console.ReadKey();
        }

        private static void bg_worker_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
                Console.WriteLine("User Cancelled Thread !!");
            else if (e.Error != null)
                Console.WriteLine("Worker Exception => {0}", e.Error);
            else
                Console.WriteLine(e.Result);
        }
        private static void bg_worker_Progress(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine(" {0}% progress... ", e.ProgressPercentage );
        }
     
    }

    internal class Args
    {
        public int _counter;
        public BackgroundWorker _bgWorker;

        public Args(int counter, BackgroundWorker bgWorker)
        {
            _counter = counter;
            _bgWorker = bgWorker;
        }
    }
}