﻿using System;
using System.IO;
using System.Threading;

namespace ReaderWriterTLockss
{
    internal class Program
    {
        private static ReaderWriterLock rw = new ReaderWriterLock();
        private static Stream stream = new FileStream("sharedFile.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
        private static Random random;
        private static int wait = 10000;

        static void Main(string[] args)
        {
            Thread[] t = {new Thread(Append), new Thread(FindAppend)};//, new Thread(Remove), new Thread(Read)};

            random = new Random();

            foreach (Thread thread in t)
            {
                thread.IsBackground = true;
                thread.Start();
            }

            Console.ReadKey();
        }

        private static void Append()
        {
            while (true)
            {
                rw.AcquireWriterLock(wait);
                TextWriter writer = new StreamWriter(stream);

                writer.Write(GetRandomNum() + " ");
                writer.Flush();

                rw.ReleaseWriterLock();
                Thread.Sleep(30);
            }
        }

        private static void FindAppend()
        {
            while (true)
            {
                rw.AcquireReaderLock(wait);
                TextReader reader = new StreamReader(stream);

                //find
                string numReplace = 2.ToString();//GetRandomNum().ToString();
                bool found = false;

                int c = reader.Peek();
                while (c != -1)
                {
                    var cc = (char) c;

                    if (cc.ToString() == numReplace)
                    {
                        found = true;
                        break;
                    }
                    c = reader.Read();
                }

                if (found)
                {
                    //replace
                    LockCookie cookie = rw.UpgradeToWriterLock(wait);
                    TextWriter writer = new StreamWriter(stream);
                    writer.Write("Found\n");
                    Console.WriteLine("Found");
                    writer.Flush();
                    rw.DowngradeFromWriterLock(ref cookie);
                }

                rw.ReleaseReaderLock();
                Thread.Sleep(50);
            }
        }

        private static void Remove()
        {
            while (true)
            {
                rw.AcquireWriterLock(wait);

                stream.Position = 0;

                while (stream.CanRead)
                {
                    var c = (char) stream.ReadByte();
                    long pos = stream.Position;

                    if (c == GetRandomNum())
                    {
                        stream.WriteByte((byte) 'F');
                        break;
                    }
                }

                rw.ReleaseWriterLock();
                Thread.Sleep(50);
            }
        }

        private static void Read()
        {
            while (true)
            {
                rw.AcquireReaderLock(wait);
                stream.Position = 0;
                TextReader reader = new StreamReader(stream);
                Console.WriteLine("Reader => length => " + reader.ReadToEnd().Length);
                rw.ReleaseReaderLock();
                Thread.Sleep(wait);
            }
        }

        private static int GetRandomNum()
        {
            lock (random)
                return random.Next(0, 9);
        }
    }
}