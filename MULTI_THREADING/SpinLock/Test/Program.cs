﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace Test
{
    class Program
    {
        private static ReaderWriterLock rw;
        private static Stream stream;
        private static Random random;
        private static int wait = 10000;

        static void Main(string[] args)
        {
            rw = new ReaderWriterLock();
            stream = new FileStream("sharedFile.txt", FileMode.Append, FileAccess.ReadWrite);
        }
    }
}
