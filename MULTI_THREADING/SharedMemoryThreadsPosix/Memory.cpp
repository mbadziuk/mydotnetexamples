#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <time.h>
#include "imports\pthread.h"
#include "conio.h"
#include <string>

#define BUF_SIZE 1024;

//  ------ Global var ----------
char shMemName[] = "Global//MySharedMemoryObject";
pthread_mutex_t shMemSyncMutex = PTHREAD_MUTEX_INITIALIZER;



void *print_message_function( void *ptr );
void *start_on_signal( void *ptr );

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t signal = PTHREAD_COND_INITIALIZER;
int counter = 0;


bool createSharedMemory(char* s);
void readSharedMemory();
bool isOk(void*);

// Shared memory test
int main(int argc, char* argv[]) {
  HANDLE hSharedMemory;
	LPVOID sharedMemPointer;
	int bufSize = BUF_SIZE;
  bool isSharedMemoryCreator = atoi(argv[1]) == 0 ? true : false;


  if (isSharedMemoryCreator) { // host
    hSharedMemory = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, bufSize, (LPCWSTR)shMemName);
  
    if (!isOk(hSharedMemory)) return false;

    sharedMemPointer = MapViewOfFile(hSharedMemory, FILE_MAP_ALL_ACCESS, 0,0, bufSize);

    if (!isOk(sharedMemPointer)) return false;

    while (true) { // write cycles
      char input[1024];
      printf("\nMessage to broadcast => ");
      scanf("%s", &input);

      pthread_mutex_lock(&shMemSyncMutex);
      CopyMemory(sharedMemPointer, input, strlen(input));
      pthread_mutex_unlock(&shMemSyncMutex);
    }
  } else {    // client
     hSharedMemory = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, (LPCWSTR)shMemName);
  
    if (!isOk(hSharedMemory)) return false;

    sharedMemPointer = MapViewOfFile(hSharedMemory, FILE_MAP_ALL_ACCESS, 0,0, bufSize);

    if (!isOk(sharedMemPointer)) return false;

    while (true) { // read cycles
      pthread_mutex_lock(&shMemSyncMutex);

      if (*((char*)sharedMemPointer) != '\0') {
        printf("\n <==  ");
        for(int i = 0;((char*)sharedMemPointer)[i] != '\0'; i++) {
          printf("%c",((char*)sharedMemPointer)[i]);
        }
        *((char*)sharedMemPointer) = '\0';
      }

      pthread_mutex_unlock(&shMemSyncMutex);
      Sleep(1000);
    }
  }
  
  UnmapViewOfFile(sharedMemPointer);
  
  if (atoi(argv[0]) == 0) {
    CloseHandle(hSharedMemory);
  }
  	
	return 0;
}

bool isOk(void* ptr) {
	bool result = true;
	if (ptr == NULL) {
		printf("Failed to work with Shared Memory.");
		result = false;
	}
	return result;
}

void readSharedMemory() {

}

int ThreadCreator() {
     pthread_t thread1, thread2;
	   pthread_t thread3;
     const char *message1 = "Thread 1";
     const char *message2 = "Thread 2";
     int  iret1, iret2;

    /* Create independent threads each of which will execute function */

     pthread_condattr_ 


     iret1 = pthread_create( &thread1, NULL, print_message_function, (void*) message1);
     if(iret1) {
         fprintf(stderr,"Error - pthread_create() return code: %d\n",iret1);
         exit(EXIT_FAILURE);
     }

     iret2 = pthread_create( &thread2, NULL, start_on_signal, (void*) message2);
     if(iret2) {
         fprintf(stderr,"Error - pthread_create() return code: %d\n",iret2);
         exit(EXIT_FAILURE);
     }

	 iret2 = pthread_create( &thread3, NULL, start_on_signal, (void*)message2);

     printf("pthread_create() for thread 1 returns: %d\n",iret1);
     printf("pthread_create() for thread 2 returns: %d\n",iret2);

     /* Wait till threads are complete before main continues. Unless we  */
     /* wait we run the risk of executing an exit which will terminate   */
     /* the process and all threads before the threads have completed.   */

     pthread_join( thread1, NULL);
     pthread_join( thread2, NULL); 
	 pthread_join( thread3, NULL); 

	printf("\n\n%d", counter);

	getchar();
	//pthread_exit(&iret1);
}

void *print_message_function( void *ptr )
{
    pthread_t id = pthread_self(); 
	
	char *message;
	message = (char *) ptr;
	 
	for (int i = 0; i < 10; i++) {
		
		//pthread_mutex_lock(&mutex); // critical section
		
		while (pthread_mutex_trylock(&mutex));

		counter++;
		//printf("%s -> id -> %d\n", message, id.p);

		if (i == 5) {
			pthread_cond_broadcast(&signal);
		}

		pthread_mutex_unlock(&mutex);

		Sleep(1000);
	}
	printf("\n\nCounting finished\n\n");

	return NULL;
}

void *start_on_signal( void *ptr )
{
    pthread_t id = pthread_self(); 
	SYSTEMTIME t;
	GetSystemTime(&t);
	timespec time;
	time.tv_sec = t.wSecond;
	time.tv_nsec = time.tv_sec * 1000000;
	time.tv_sec += 2;
	
	char *message;
	message = (char *) ptr;
	
	pthread_mutex_lock(&mutex);
	//pthread_cond_wait(&signal, &mutex);
	pthread_cond_timedwait(&signal, &mutex, &time);

	printf("\n\nReceived a signal from other thhread &d\n\n", id);
	printf("\n\nCounter == %d\n\n", counter);

	pthread_mutex_unlock(&mutex);

	return NULL;
}