﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TPL
{
    class Worker
    {

        public int Run(object a)
        {
            Console.WriteLine("Task started {0}", Task.CurrentId);
            Console.WriteLine("Calculating.");
            for (int i = 0; i < 20; i++)
            {
                Console.Write(".");
                Thread.Sleep(50);
            }
            Console.WriteLine("Task completed {0}", Task.CurrentId);


            return ((Program.Argss)a).a1 + ((Program.Argss)a).b1;
        }
    }


    class Program
    {
        public struct Argss
        {
            public int a1;
            public int b1;
        }

        static void Main(string[] args)
        {
            // Создаем экземпляр класса
            Worker worker = new Worker();

            // Создаем делегат указатель на метод экземпляра

            Argss a;
            a.a1 = 5;
            a.b1 = 7;


            Task<int> runner = Task<int>.Factory.StartNew(worker.Run, a);

            Console.WriteLine(runner.Result);
            
            Console.ReadKey();
            
        }
    }
}
