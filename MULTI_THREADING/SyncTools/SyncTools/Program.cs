﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;

namespace SyncTools
{
    class Program
    {
        public static int callBackCallsCounter = 0;

        static void Main(string[] args)
        {
            int a = 10;
            int b = 5;

            // Using IAsyncResult model for background async method calling
            AsyncCallback callback = new AsyncCallback(Callback);
            Func<int, int, int> asyncWorker = new Func<int, int, int>(WorkAsync);

            for (int i = 0; i < a; i++)
            {
                asyncWorker.BeginInvoke( a, b, callback, null );    
            }
            
            for (int i = 0; i < 20; i++)
            {
                Console.Write("M");
                Thread.Sleep(10);
            }
            Console.WriteLine("Main Work Finished.. Wait for Async Worker result...");


            Console.ReadKey();

            Console.WriteLine(callBackCallsCounter);

        }

        private static void Callback(IAsyncResult ar)
        {
            var res = ar as AsyncResult;
            var res1 = (Func<int,int,int>)res.AsyncDelegate;
            int result = res1.EndInvoke(ar);
            Console.WriteLine("Callback Result {0}  !!!", result);

            callBackCallsCounter++;
        }

        public static int WorkAsync(int a, int b)
        {
            Console.WriteLine("Async work started !");

            for (int i = 0; i < 50; i++)
            {
                Console.Write(".");
                Thread.Sleep(50);
            }

            Console.WriteLine("Async work finished !");

            return a + b;
        }


        class Worker
        {
            private readonly EventWaitHandle _signal;
            private readonly EventWaitHandle _isComplete;

            private readonly string _name;


            public Worker(EventWaitHandle signal, string name, EventWaitHandle isComplete)
            {
                _signal = signal;
                _name = name;
                _isComplete = isComplete;
                ThreadPool.QueueUserWorkItem(Run, this);
            }

            private void Run(object state)
            {
                Worker thiso = (Worker)state;
                Thread.CurrentThread.Name = thiso._name;

                while (true)
                {
                    Console.WriteLine("Red Light. Thread {0} waits on signal.", Thread.CurrentThread.Name);

                    _signal.WaitOne();

                    Console.WriteLine(" Green Light. Signal from main Thread Received at Thread {0}. ", Thread.CurrentThread.Name);
                    Console.Write(" Thread {0} is Hardly working. ", Thread.CurrentThread.Name);

                    thiso._signal.Reset();

                    for (int i = 0; i < 10; i++)
                    {
                        Console.Write(".");
                        Thread.Sleep(200);
                    }
                    Console.WriteLine(" Thread {0} work complete. And send signal to Main Thread.", Thread.CurrentThread.Name);
                    Console.WriteLine();
                    thiso._isComplete.Set();
                }
            }
        }
        private static void SignalSync()
        {
            ManualResetEvent signal = new ManualResetEvent(false);
            AutoResetEvent isComplete = new AutoResetEvent(false);
            Worker[] arayWorkers = new Worker[3];

            Console.WriteLine("Creating workers!");

            for (int i = 0; i < 3; i++)
            {
                arayWorkers[i] = new Worker(signal, i.ToString(), isComplete);
            }
            Console.WriteLine();
            Console.WriteLine("Press any key to release one of workers !!!");
            Console.WriteLine();


            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey();

                if (key.Key == ConsoleKey.Q)
                    break;

                if (key.Key == ConsoleKey.S)
                {
                    signal.Set();
                    //isComplete.WaitOne();
                    Console.WriteLine("Main Thread Recieved feedback Complete signal.");
                }
            }
        }
    }
}
