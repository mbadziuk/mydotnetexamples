﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApplication1
{
    class Program
    {
        public static long DiseaseCount;

        public static void AddSickPerson()
        {
            while (true)
            {
                Interlocked.Increment(ref DiseaseCount);
                var now = Interlocked.Read(ref DiseaseCount);
                Console.WriteLine("Add 1 - now  {0}", now);
                
                if (now > 10)
                    break;

                Thread.Sleep(new Random().Next(50, 500));
            }
        }

        public static void DecrementSickPerson()
        {
            while (Interlocked.Read(ref DiseaseCount) < 10)
            {
                Interlocked.Decrement(ref DiseaseCount);
                var now = Interlocked.Read(ref DiseaseCount);
                Console.WriteLine("Dec 1 - now  {0}", now);
                Thread.Sleep(new Random().Next(300, 600));
            }
        }
        
        static void Main(string[] args)
        {
            Thread thread1 = new Thread(AddSickPerson);
            Thread thread2 = new Thread(DecrementSickPerson);

            thread1.Priority = ThreadPriority.Highest;
            thread2.Priority = ThreadPriority.Lowest;
            
            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();

            Thread.Sleep(10000);

            Console.WriteLine("Main thread finished..");

        }
    }
}
