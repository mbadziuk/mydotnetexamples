﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskTest
{

    class Program
    {
       
        static void Main(string[] args)
        {
            var delegat = new Action<int,int,object>(Run);
            CancellationTokenSource tokenSrc = new CancellationTokenSource();

            Task task = new Task(()=> delegat(5,6,tokenSrc),tokenSrc.Token);
            task.Start();

            Thread.Sleep(2000);

            try
            {
                tokenSrc.Cancel();
                task.Wait();
            }
            catch (Exception)
            {
                Console.WriteLine();
                Console.WriteLine("Potok cancelled successfully!");
            }
            finally
            {
                task.Dispose();
                tokenSrc.Dispose();
            }

            Console.ReadKey();

        }

        static void Run(int a, int b, object o)
        {
            Console.WriteLine("Startanyl potok !");

            CancellationToken token = (CancellationToken) o;

            if (token.IsCancellationRequested)
                token.ThrowIfCancellationRequested();

            Console.Write("Potok Working.");
            for (int i = 0; i < 100; i++)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Releasing potok resources..");
                    for (int j = 0; j < 20; j++)
                    {
                        Console.Write("-");
                        Thread.Sleep(20);
                    }

                    Console.WriteLine("Result = {0}", a+b);
                    token.ThrowIfCancellationRequested();
                }
                
                Console.Write(".");
                Thread.Sleep(100);
            }

            Console.WriteLine("Zakonchilsya potok !");
        }

    }
}
