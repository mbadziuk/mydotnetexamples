Instruction of HOWTO create a target of BnrCtlL32 for Linux with JNA defines and TAG it in SVN:

Linux login: bnr
Linux password: bnr

1) checkout last version of BntCtlL32 to shared with Linux directory
2) mount a shared folder Win-Linux in Linux by typing in shell
    
    sudo mount -t vboxsf share /media/shared  (last 2 -> name of shared dir in host OS, name of dir in guest OS)  

3) open eclipse with root rights
    
    sudo eclipse

4) open C/C++ perspective (in perspective tabs choose C++)

5)  change code (for JNA compliance no pragma packing)
	
	in /imports/common/src/api
		
		change BnrCtlL32.h - line 96 in WIN32  section
			OLD -  #define CALLTYPE __stdcall
			NEW -   #ifdef BNRCTLJNA
				#define CALLTYPE
				#else
				#define CALLTYPE __stdcall
				#endif	

	in /imports/common/src/api
		
		change BnrHist32.h - line 116 and 1324 in WIN32  section
			OLD -  // Set 2 bytes struct member aligment 
				 #ifndef WIN32
				#pragma pack(push,2)
				#else
				#include <pshpack2.h>
				#endif

			NEW -  // Set 2 bytes struct member aligment line 116 	   // restore packing on line 1324 
				#ifndef BNRCTLJNA
				#ifndef WIN32
				#pragma pack(push,2)
				#else
				#include <pshpack2.h>
				#endif
				#endif
	in /imports/common/src/api
		
		change BnrProp32.h - line 123 and 2469 in WIN32  section  -> same way as above


	in /imports/common/src/meireserved
		
		change BnrCtlMeiReserved.h - line 49 in WIN32  section
			
			OLD -  #define CALLTYPE __stdcall
			NEW -   #ifdef BNRCTLJNA
				#define CALLTYPE
				#else
				#define CALLTYPE __stdcall
				#endif	

		change BnrCtlMeiReserved.h - line 62 and in WIN32  section for 2 bytes struct alignment
				
			OLD - // Set 2 bytes struct member aligment 
				#ifndef WIN32
				#pragma pack(push,2)
				#else
				#include <pshpack2.h>
				#endif

			NEW -  // Set 2 bytes struct member aligment line 62 	   // restore packing on line 819		
				#ifndef BNRCTLJNA
				#ifndef WIN32
				#pragma pack(push,2)
				#else
				#include <pshpack2.h>
				#endif
				#endif

6) create new target for building JNA 
	a) RMC (right-mouse-click) on Project -> Build Configs -> Manage -> create new Config with default values from Release
	b) change Build Variable - VERSION - to correct number
	c) build this config -> copy libs to /output/x86/ReleaseJna folder

7) create branch

8) switch to branch 

9) create tag


**************************************************

Testing in Linux:

1) copy libBnrCtlL32Jna.so.1.10.1 to /usr/lib folder

	cp libBnrCtlL32Jna.so.1.10.1 /usr/lib/libBnrCtlL32Jna.so.1.10.1

   check that all other libs -> xmpRpc etc.. are copied there as well

2) create link to it

	ln -sf libBnrCtlL32Jna.so.1.10.1 libBnrCtlL32Jna.so

3) copy all BnrCtlJava project to shared with linux folder   (on my computer  c:\share)

4) open eclipse on Linux and import BnrCtlJava project to it

5) 
