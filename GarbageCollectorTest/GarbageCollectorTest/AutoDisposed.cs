﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GarbageCollectorTest
{
    // Auto and Manual Disposable class template ))
    class AutoDisposed: IDisposable
    {
        private bool _isDisposed = false;
        private FileStream stream;

        public AutoDisposed()
        {
            stream = new FileStream("SomeTestFile.txt", FileMode.Append);
            Console.WriteLine("Opening Filestream...");
        }

        ~AutoDisposed()
        {
            Console.WriteLine("Destructor of Unmanaged Resource autocall");
            Dispose();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;
            _isDisposed = true;

            Console.WriteLine("Dispose method called )))");
            Console.WriteLine("Dispose of unmanaged resource!!! ");
            Console.WriteLine("Closing FileStream...");
            
            stream.Close();  
            GC.SuppressFinalize(this);
        }

        public void AddToFile(string text)
        {
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(text);
            }
        }
    }
}
