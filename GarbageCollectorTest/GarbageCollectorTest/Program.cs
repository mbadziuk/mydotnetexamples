﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GarbageCollectorTest
{
    /*
     * 
     * 
     * 
     * 
Создайте свой класс, объекты которого будут занимать много места в памяти
 (например, в коде класса будет присутствовать большой массив) 
     * и реализуйте для этого класса формализованный шаблон очистки. 

Создайте класс, который позволит выполнять мониторинг ресурсов, используемых программой. 
Используйте его в целях наблюдения за работой программы, а именно: пользователь может указать приемлемые уровни потребления ресурсов (памяти),
     * а методы класса позволят выдать предупреждение, когда количество реально используемых ресурсов приблизиться к максимально допустимому уровню. 

     * 
     */
    class Program
    {
        static void Main(string[] args)
        {
//            AutoDisposed a = new AutoDisposed();
//
//            a.AddToFile("sdasdsad");
//
//            Console.ReadKey();
                        

            GCTests();
        }

        private static void GCTests()
        {
            Console.WriteLine("Program started.. Memory used :: " + GC.GetTotalMemory(false)/1000000 + " Mb");
            Console.WriteLine("Program started.. Generating bigObjects...");
            int count = 1000;
            BigData[] bigObjectAr = new BigData[count];

            try
            {
                for (int i = 0; i < count; i++)
                {
                    bigObjectAr[i] = new BigData(); //400 GB
//                    bigObjectAr[i].SomeMethod(i);
                }
            }
            catch (OutOfMemoryException ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.WriteLine("Управляемая куча ПЕРЕПОЛНЕНА!");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
           
            Console.WriteLine();
            Console.WriteLine("Generating bigObjects complete.");
            Console.WriteLine("Program work.. Memory used :: " + GC.GetTotalMemory(false)/1000000 + " Mb");

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(" Count collections in {0} gen = {1}", i, GC.CollectionCount(i));
            }
           

            Console.ReadKey();
            Console.WriteLine("MAx geneartion => " + GC.MaxGeneration);
            bigObjectAr = null;
//            Console.ReadKey();
            Console.WriteLine("Reference null.. Memory used :: " + GC.GetTotalMemory(false)/1000000 + " Mb");
//            Console.ReadKey();

            GC.Collect();
            Console.WriteLine("AFter collection => Memory used :: " + GC.GetTotalMemory(false)/1000000 + " Mb");

//            Console.WriteLine("Start disposing bigObjects.");
//            for (int i = 0; i < count; i++)
//            {
//                bigObjectAr[i].Dispose();
//            }
//
//            Console.WriteLine("Disposing bigObjects complete.");
            Console.ReadKey();
        }
    }

    class BigData: IDisposable
    {
        private int _length;
        private int[] _array;

        public BigData(int length = 100000000)
        {
            _length = length;
            _array = new int[length]; // 400 mb

            for (int i = 0; i < length; i++)
            {
                _array[i] = i;
            }
        }

        public void Dispose()
        {
            _array = null;
        }

        public void SomeMethod(int i)
        {
            Console.WriteLine(i);
        }
    }
}
